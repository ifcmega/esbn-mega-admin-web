var endpoints = location.origin;
var emailLogController = angular
		.module('emailLogController', [])
		.config(
				[
						'$httpProvider',
						function($httpProvider) {
							$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
						} ]).directive('format', ['$filter', function ($filter) {
						    return {
					 	        require: '?ngModel',
						        link: function (scope, elem, attrs, ctrl) {
						            if (!ctrl) return;


						            ctrl.$formatters.unshift(function (a) {
						                return $filter(attrs.format)(ctrl.$modelValue)
						            });


						            ctrl.$parsers.unshift(function (viewValue) {
						                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
						                elem.val($filter(attrs.format)(plainNumber));
						                return plainNumber;
						            });
						        }
						    };
						}])
						.directive('nonNulldrop', function () {
	        return {
	            require: 'ngModel',
	            link: function (scope, element, attributes, control) {
	                control.$validators.adult = function (modelValue, viewValue) {
                         
                         if(viewValue==undefined){
                         $(element).css({"border-color" : "red"})
                         }
                         else{
                         $(element).css({"color":"black","border-color" : "green"}) 
                         return true
                         }
	                };
	            }
	        };
	    });

emailLogController.$inject = ['$scope'];
emailLogController.directive("datepicker", function () {
    return {
        restrict: "A",
        link: function (scope, el, attr) {
            el.datepicker({
                            dateFormat: 'dd/mm/yy'
                        });
        }
    };
})


var config = {
	headers : {
		'Accept' : 'application/json, */*'
	}
};

emailLogController.controller('emailLog', function($scope, $http,
		$rootScope, $location, $window) {
	//debugger;
	$scope.sortBy = 'logId';
	$scope.sortCondition = 'DESC';
	$scope.search = '';
	$scope.status = '';
	$scope.subject = '';
	$scope.searchKolom = '';
	$scope.searchInput='';

	
	
	$scope.searchMyData = function(page,inputan){
		$scope.search = inputan;
		if($scope.fromDate == undefined){
			$scope.fromDate = 'fromDate';
		}
		if($scope.toDate == undefined){
			$scope.toDate = 'toDate';
		}
		if($scope.sortBy == undefined){
			$scope.sortBy = 'logId';	
		}
		if($scope.sortCondition == undefined){
			$scope.sortCondition = 'DESC';	
		}
				
		//debugger;
		$http.post("emailLogWsCtl/get-email-log",
				{
			           search : inputan,
			           searchKolom : $scope.kolom, 
			           page : page,
			           sortBy : $scope.sortBy,
			           sortCondition : $scope.sortCondition,
			           fromDate : $scope.fromDate,
			           status : $scope.status,
			           toDate : $scope.toDate,
			           subject : $scope.subject,
				})
		.then(
				function(response) {
				//debugger;
				if($scope.fromDate == 'fromDate'){
					$scope.fromDate = undefined;
				}
				if($scope.toDate == 'toDate'){
					$scope.toDate = undefined;
				}
				
					if (response.data.isError == true) {
						alert(response.data.reason);
					} else {
						$scope.listEmailLog = response.data.content;
						$scope.pageNumber = response.data.pagination.page;
						$scope.totalPage = response.data.totalPage;
						$scope.total = response.data.totalData;
					}
				},
				function error(response) {
					//debugger;
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
					$scope.itsWinningSave.invalidModal = true;
					$scope.itsWinningSave.waitModal = false;
				});	
	}
	
	$scope.searchMyFilterData = function(page){
		if($scope.fromDate == undefined){
			$scope.fromDate = 'fromDate';
		}
		if($scope.toDate == undefined){
			$scope.toDate = 'toDate';
		}
		if($scope.sortBy == undefined){
			$scope.sortBy = 'logTglWaktu';	
		}
		if($scope.sortCondition == undefined){
			$scope.sortCondition = 'DESC';	
		}
		
		//debugger;
		$http.post("emailLogWsCtl/get-email-log",
				{
			           search : $scope.search,
			           searchKolom : $scope.kolom, 
			           page : page,
			           sortBy : $scope.sortBy,
			           sortCondition : $scope.sortCondition,
			           fromDate : $scope.fromDate,
			           status : $scope.status,
			           toDate : $scope.toDate,
				})
		.then(
				function(response) {
				//debugger;
				if($scope.fromDate == 'fromDate'){
					$scope.fromDate = undefined;
				}
				if($scope.toDate == 'toDate'){
					$scope.toDate = undefined;
				}
				
					if (response.data.isError == true) {
						alert(response.data.reason);
					} else {
						$scope.listEmailLog = response.data.content;
						$scope.pageNumber = response.data.pagination.page;
						$scope.totalPage = response.data.totalPage;
						$scope.total = response.data.totalData;
					}
				},
				function error(response) {
					//debugger;
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
					$scope.itsWinningSave.invalidModal = true;
					$scope.itsWinningSave.waitModal = false;
				});	
	}
	
	$scope.searchMyData(0,'');
	
});

