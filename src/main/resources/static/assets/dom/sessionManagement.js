var endpoints = location.origin;
var sessionManagementController = angular
		.module('sessionManagementController', [])
		.config(
				[
						'$httpProvider',
						function($httpProvider) {
							$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
						} ])
		.directive(
				'format',
				[
						'$filter',
						function($filter) {
							return {
								require : '?ngModel',
								link : function(scope, elem, attrs, ctrl) {
									if (!ctrl)
										return;
									ctrl.$formatters.unshift(function(a) {
										return $filter(attrs.format)(
												ctrl.$modelValue)
									});
									ctrl.$parsers.unshift(function(viewValue) {
										var plainNumber = viewValue.replace(
												/[^\d|\-+|\.+]/g, '');
										elem.val($filter(attrs.format)(
												plainNumber));
										return plainNumber;
									});
								}
							};
						} ])
		//validasi untuk input						
		.directive(
				'nonNull',
				function() {
					return {
						require : 'ngModel',
						link : function(scope, element, attributes, control) {
							control.$validators.adult = function(modelValue,
									viewValue) {

								if (viewValue == undefined) {
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								} else {
									$(element).css({
										"color" : "black",
										"border-color" : "green"
									})
									return true
								}
							};
						}
					};
				})
		//validasi untuk drop down
		.directive(
				'nonNulldrop',
				function() {
					return {
						require : 'ngModel',
						link : function(scope, element, attributes, control) {
							control.$validators.adult = function(modelValue,
									viewValue) {

								if (viewValue == undefined) {
									$(element).css({
										"border-color" : "red"
									})
								} else {
									$(element).css({
										"color" : "black",
										"border-color" : "green"
									})
									return true
								}
							};
						}
					};
				})

		.directive(
				'nominalValidation',
				function() {
					return {
						require : 'ngModel',
						link : function(scope, element, attributes, control) {
							control.$validators.adult = function(modelValue,
									viewValue) {
								var minPemesanan = parseInt(document
										.getElementsByName("minPemesanan")[0].value
										.replace(/,/g, ''));
								var maxPemesanan = parseInt(document
										.getElementsByName("maxPemesanan")[0].value
										.replace(/,/g, ''));
								var kelipatan = parseInt(document
										.getElementsByName("kelipatan")[0].value
										.replace(/,/g, ''));

								if (modelValue == undefined)
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								else if (isNaN(minPemesanan)
										&& isNaN(maxPemesanan))
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								else if (parseInt(modelValue) > maxPemesanan
										|| parseInt(modelValue) < minPemesanan)
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								else if (modelValue % parseInt(kelipatan) != 0)
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								else {
									$(element).css({
										"color" : "black",
										"border-color" : "green"
									})
									return true;
								}
							};
						}
					};
				});

sessionManagementController.$inject = [ '$scope' ];

var config = {
	headers : {
		'Accept' : 'application/json, */*'
	}
};

sessionManagementController.controller('sessionManagement', function($scope,
		$http, $rootScope, $location, $window) {
	
	
	$scope.selectedKolom = function(){
		$scope.kolom;
		debugger;
	}
	
	$scope.descFunction = function(sort,kolomSort){
		$scope.sort = sort;
		$scope.kolomSort = kolomSort;
		$http.post("maintenaceSessions/retrieve-sort",
				{
			          kolom : $scope.kolom,
					  search : $scope.searchEngine,
					  page : 1,
					  sort : $scope.sort,
					  kolomSort : $scope.kolomSort
				})
		.then(
				function(response) {
				debugger;
				$scope.listSessions = response.data.data;
				$scope.pageNumber = response.data.pageNumber;
				$scope.totalPage = response.data.totalPage;
				$scope.total = response.data.total;
				},
				function error(response) {
					debugger;
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
					$scope.itsWinningSave.invalidModal = true;
					$scope.itsWinningSave.waitModal = false;
				});	
	}
	
	$scope.searchFunction = function(searchEngine){
		if($scope.sort==undefined){
			$scope.sort = '';
		}

		if( $scope.kolomSort==undefined){
			$scope.sort = '';
		}
		$http.post("maintenaceSessions/retrieve-search",
				{
			          kolom : $scope.kolom,
					  search : searchEngine,
					  page : 1,
					  sort : $scope.sort,
					  kolomSort : $scope.kolomSort
				})
		.then(
				function(response) {
				debugger;
				$scope.listSessions = response.data.data;
				$scope.pageNumber = response.data.pageNumber;
				$scope.totalPage = response.data.totalPage;
				$scope.total = response.data.total;
				},
				function error(response) {
					debugger;
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
					$scope.itsWinningSave.invalidModal = true;
					$scope.itsWinningSave.waitModal = false;
				});	
	}
	
	$scope.pageFunction = function(pageNumber){
		if($scope.sort==undefined){
			$scope.sort = '';
		}

		if( $scope.kolomSort==undefined){
			$scope.sort = '';
		}
		$http.post("maintenaceSessions/retrieve-paging",
				{
			          kolom : $scope.kolom,
					  search : $scope.searchEngine,
					  page : pageNumber,
					  sort : $scope.sort,
					  kolomSort : $scope.kolomSort
				})
		.then(
				function(response) {
				debugger;
				$scope.listSessions = response.data.data;
				$scope.pageNumber = response.data.pageNumber;
				$scope.totalPage = response.data.totalPage;
				$scope.total = response.data.total;
				},
				function error(response) {
					debugger;
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
					$scope.itsWinningSave.invalidModal = true;
					$scope.itsWinningSave.waitModal = false;
				});
		
	}
	
	$scope.killMeSession = function(){
		$http.get("maintenaceSessions/killedSession")
		.then(
				function(response) {
				debugger;
			
				},
				function error(response) {
					debugger;
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
					$scope.itsWinningSave.invalidModal = true;
					$scope.itsWinningSave.waitModal = false;
				});
		
	};
	
	$scope.sessionUser = function(){
	
	$http.get("maintenaceSessions/retrieve")
	.then(
			function(response) {
			debugger;
			$scope.listSessions = response.data.data;
			$scope.pageNumber = response.data.pageNumber;
			$scope.totalPage = response.data.totalPage;
			$scope.total = response.data.total;
			},
			function error(response) {
				debugger;
				$scope.postResultMessage = "Error with status: "
						+ response.statusText;
				$scope.itsWinningSave.invalidModal = true;
				$scope.itsWinningSave.waitModal = false;
			});
	
	}
	
	$scope.sessionUser();
});
