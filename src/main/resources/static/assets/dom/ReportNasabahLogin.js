var endpoints = location.origin;
var reportUserLoginController = angular
		.module('reportUserLoginController', [])
		.config(
				[
						'$httpProvider',
						function($httpProvider) {
							$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
						} ])

		.directive(
				'format',
				[
						'$filter',
						function($filter) {
							return {
								require : '?ngModel',
								link : function(scope, elem, attrs, ctrl) {
									if (!ctrl)
										return;
									ctrl.$formatters.unshift(function(a) {
										return $filter(attrs.format)(
												ctrl.$modelValue)
									});
									ctrl.$parsers.unshift(function(viewValue) {
										var plainNumber = viewValue.replace(
												/[^\d|\-+|\.+]/g, '');
										elem.val($filter(attrs.format)(
												plainNumber));
										return plainNumber;
									});
								}
							};
						} ])
		//validasi untuk input						
		.directive(
				'nonNull',
				function() {
					return {
						require : 'ngModel',
						link : function(scope, element, attributes, control) {
							control.$validators.adult = function(modelValue,
									viewValue) {

								if (viewValue == undefined) {
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								} else {
									$(element).css({
										"color" : "black",
										"border-color" : "green"
									})
									return true
								}
							};
						}
					};
				})
		//validasi untuk drop down
		.directive(
				'nonNulldrop',
				function() {
					return {
						require : 'ngModel',
						link : function(scope, element, attributes, control) {
							control.$validators.adult = function(modelValue,
									viewValue) {

								if (viewValue == undefined) {
									$(element).css({
										"border-color" : "red"
									})
								} else {
									$(element).css({
										"color" : "black",
										"border-color" : "green"
									})
									return true
								}
							};
						}
					};
				})

		.directive(
				'nominalValidation',
				function() {
					return {
						require : 'ngModel',
						link : function(scope, element, attributes, control) {
							control.$validators.adult = function(modelValue,
									viewValue) {
								var minPemesanan = parseInt(document
										.getElementsByName("minPemesanan")[0].value
										.replace(/,/g, ''));
								var maxPemesanan = parseInt(document
										.getElementsByName("maxPemesanan")[0].value
										.replace(/,/g, ''));
								var kelipatan = parseInt(document
										.getElementsByName("kelipatan")[0].value
										.replace(/,/g, ''));

								if (modelValue == undefined)
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								else if (isNaN(minPemesanan)
										&& isNaN(maxPemesanan))
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								else if (parseInt(modelValue) > maxPemesanan
										|| parseInt(modelValue) < minPemesanan)
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								else if (modelValue % parseInt(kelipatan) != 0)
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								else {
									$(element).css({
										"color" : "black",
										"border-color" : "green"
									})
									return true;
								}
							};
						}
					};
				});

reportUserLoginController.$inject = [ '$scope' ];
reportUserLoginController.directive("datepicker", function () {
    return {
        restrict: "A",
        link: function (scope, el, attr) {
            el.datepicker({
                            dateFormat: 'dd/mm/yy'
                        });
        }
    };
})
reportUserLoginController
		.config([
				'$compileProvider',
				function($compileProvider) {
					$compileProvider
							.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension):/);
					// Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
				} ]);

var config = {
	headers : {
		'Accept' : 'application/json, */*'
	}
};

reportUserLoginController.controller('reportUserLogin', function($scope, $http,
		$rootScope, $location, $window) {
	$scope.myData = {};
	$scope.retrieveReport = function(page) {
		if($scope.myData.startDate == undefined){
			$scope.myData.startDate = 'minDate';
		}
		if($scope.myData.endDate == undefined){
			$scope.myData.endDate = 'maxDate';
		}
		if($scope.myData.kolom == undefined){
			$scope.myData.kolom = '%%'; 
		}
		$http.post(
				"reportUserLoginWsCtl/retrieve-all-report/nasabah?page=" + page,
				{
					'userType' : $scope.myData.kolom,
					'startDate' : $scope.myData.startDate,
					'endDate' : $scope.myData.endDate
				}).then(function(response) {
		
			if($scope.myData.startDate == 'minDate'){
				$scope.myData.startDate = undefined;
			}
			if($scope.myData.endDate == 'maxDate'){
				$scope.myData.endDate = undefined;
			}
			$scope.totalSistemLogin = response.data.totalSistemLogin;
			$scope.totalLoginNasabah = response.data.totalLoginNasabah;
		
		}, function errorCallback(response) {
			
		});
	}
	
	$scope.search = '';
	
	$scope.retrieveReportNew = function(page) {
		if($scope.myData.startDate == undefined){
			$scope.myData.startDate = 'minDate';
		}
		if($scope.myData.endDate == undefined){
			$scope.myData.endDate = 'maxDate';
		}
		if($scope.myData.kolom == undefined){
			$scope.myData.kolom = '%%'; 
		}
		$http.post(
				"reportUserLoginWsCtl/new-retrieve-report/nasabah?page=" + page,
				{
					'userType' : $scope.myData.kolom,
					'startDate' : $scope.myData.startDate,
					'endDate' : $scope.myData.endDate,
					'search' : $scope.search
				}).then(function(response) {
			
			if($scope.myData.startDate == 'minDate'){
				$scope.myData.startDate = undefined;
			}
			if($scope.myData.endDate == 'maxDate'){
				$scope.myData.endDate = undefined;
			}
			$scope.detectedData = response.data.content;
			$scope.pageNumber = response.data.number + 1;
			$scope.totalPage = response.data.totalPages;
			$scope.total = response.data.totalElements;
		}, function errorCallback(response) {
			
		});
	}
	
	
	$scope.retrieveReportNew(1);
	$scope.retrieveReport(1);
});