var endpoints = location.origin;
var uploadUserController = angular
		.module('uploadUserController', [])
		.config(
				[
						'$httpProvider',
						function($httpProvider) {
							$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
						} ])
						
		.directive(
				'format',
				[
						'$filter',
						function($filter) {
							return {
								require : '?ngModel',
								link : function(scope, elem, attrs, ctrl) {
									if (!ctrl)
										return;
									ctrl.$formatters.unshift(function(a) {
										return $filter(attrs.format)(
												ctrl.$modelValue)
									});
									ctrl.$parsers.unshift(function(viewValue) {
										var plainNumber = viewValue.replace(
												/[^\d|\-+|\.+]/g, '');
										elem.val($filter(attrs.format)(
												plainNumber));
										return plainNumber;
									});
								}
							};
						} ])
		//validasi untuk input						
		.directive(
				'nonNull',
				function() {
					return {
						require : 'ngModel',
						link : function(scope, element, attributes, control) {
							control.$validators.adult = function(modelValue,
									viewValue) {

								if (viewValue == undefined) {
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								} else {
									$(element).css({
										"color" : "black",
										"border-color" : "green"
									})
									return true
								}
							};
						}
					};
				})
		//validasi untuk drop down
		.directive(
				'nonNulldrop',
				function() {
					return {
						require : 'ngModel',
						link : function(scope, element, attributes, control) {
							control.$validators.adult = function(modelValue,
									viewValue) {

								if (viewValue == undefined) {
									$(element).css({
										"border-color" : "red"
									})
								} else {
									$(element).css({
										"color" : "black",
										"border-color" : "green"
									})
									return true
								}
							};
						}
					};
				})

		.directive(
				'nominalValidation',
				function() {
					return {
						require : 'ngModel',
						link : function(scope, element, attributes, control) {
							control.$validators.adult = function(modelValue,
									viewValue) {
								var minPemesanan = parseInt(document
										.getElementsByName("minPemesanan")[0].value
										.replace(/,/g, ''));
								var maxPemesanan = parseInt(document
										.getElementsByName("maxPemesanan")[0].value
										.replace(/,/g, ''));
								var kelipatan = parseInt(document
										.getElementsByName("kelipatan")[0].value
										.replace(/,/g, ''));

								if (modelValue == undefined)
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								else if (isNaN(minPemesanan)
										&& isNaN(maxPemesanan))
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								else if (parseInt(modelValue) > maxPemesanan
										|| parseInt(modelValue) < minPemesanan)
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								else if (modelValue % parseInt(kelipatan) != 0)
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								else {
									$(element).css({
										"color" : "black",
										"border-color" : "green"
									})
									return true;
								}
							};
						}
					};
				});

uploadUserController.$inject = [ '$scope' ];


uploadUserController.config( [
    '$compileProvider',
    function( $compileProvider )
    {   
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension):/);
        // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
    }
]);

var config = {
	headers : {
		'Accept' : 'application/json, */*'
	}
};


uploadUserController.controller('uploadUser', function($scope,
		$http, $rootScope, $location, $window) {
	$scope.listRejected = [];
	$scope.detectedData = {};
	
	$scope.checkData = function(data,position){
		$scope.listRejected;
		var selestial = undefined;
		angular.forEach($scope.listRejected,function(list,index){
			if(position!=index){
				list.isSelected = false;
			}
			if(list.isSelected){
				selestial = data;
			}
		});
		$scope.bodyDownload = selestial;
		debugger;
	}
	$scope.pageNumber = 1;
	$scope.total = 0;
	$scope.totalPage = 1;
	$scope.downloadMe = function(){
		$http.get("userMaintenanceWsCtl/downloadableFile").then(function(response) {
			  blob = new Blob([response.data], { type: 'text/plain' }),
		        url = $window.URL || $window.webkitURL;
		    $scope.fileUrl = url.createObjectURL(blob);
			debugger;			
       	}, function errorCallback(response) {
		$window.alert("Pastikan extension File adalah .xlsx dan .xls");
	    });
	}
	
	$scope.readExcelFile = function(event) {
		debugger;
		$scope.input = event.target;
		var size = $scope.input.files[0].size;
		var reader = new FileReader();

		if (size < 393216) {
			reader.onload = function() {
				dataURL = reader.result;
				var nameUpload = $scope.input.files[0].name;
				nameUpload = nameUpload.replace(/[^0-9a-z]/gi, '');
				$scope.dataUrl = {
						name : nameUpload,
						dataUri : dataURL
				}
				$http.post("userMaintenanceWsCtl/uploadFile", $scope.dataUrl, config).then(function(response) {
						debugger;
						$scope.detectedData = response.data;
						if(response.data.totalRejected>0){
						$scope.detectedData.detectedNameDownload = nameUpload;
						$scope.detectedData.file = $scope.input.files[0].name;
						$scope.detectedData.size = $scope.input.files[0].size/1000;
						$scope.detectedData.ext = $scope.input.files[0].name.split('.')[1];
						$scope.listRejected.push($scope.detectedData);
						$scope.pageNumber = 1;
						$scope.total = $scope.listRejected.length;
						angular.element(document
								.getElementsByName("fileInput")[0]).value="";
						$window.alert('Data Yang Anda Upload Masih Salah, Anda dapat mendownload Data Tersebut Dengan Memilih Item Di Table Terlampir Lalu Klik Download Rejected');
						}
						else{
							  var r = confirm("Data Yang Anda Upload Berhasil, Apakah Anda Ingin ke Menu User List?");
							  if (r == true) {	
							  window.location = "userList";
							  }
						}
				}, function errorCallback(response) {
					$window.alert("Pastikan extension File adalah .xlsx dan .xls");
				});
				debugger;
			}
		} else {
			$window.alert('Ukuran file terlalu besar');
		}

		reader.readAsDataURL($scope.input.files[0]);
	}
});