var endpoints = location.origin;
var userManagementController = angular
		.module('userManagementController', [])
		.config(
				[
						'$httpProvider',
						function($httpProvider) {
							$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
						} ])
		.directive(
				'format',
				[
						'$filter',
						function($filter) {
							return {
								require : '?ngModel',
								link : function(scope, elem, attrs, ctrl) {
									if (!ctrl)
										return;
									ctrl.$formatters.unshift(function(a) {
										return $filter(attrs.format)(
												ctrl.$modelValue)
									});
									ctrl.$parsers.unshift(function(viewValue) {
										var plainNumber = viewValue.replace(
												/[^\d|\-+|\.+]/g, '');
										elem.val($filter(attrs.format)(
												plainNumber));
										return plainNumber;
									});
								}
							};
						} ])
		// validasi untuk input
		.directive(
				'nonNullsid',
				function() {
					return {
						require : 'ngModel',
						link : function(scope, element, attributes, control) {
							control.$validators.adult = function(modelValue,
									viewValue) {

								var pass = document
										.getElementsByName("sidValidation")[0].value;
								debugger;
								if (pass == undefined || pass == ''
										|| pass == 'true') {
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								} else {
									$(element).css({
										"color" : "black",
										"border-color" : "green"
									})
									return true
								}
							};
						}
					};
				}).directive(
				'nonNull',
				function() {
					return {
						require : 'ngModel',
						link : function(scope, element, attributes, control) {
							control.$validators.adult = function(modelValue,
									viewValue) {

								if (viewValue == undefined || viewValue == '') {
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								} else {
									$(element).css({
										"color" : "black",
										"border-color" : "green"
									})
									return true
								}
							};
						}
					};
				})
		// validasi untuk drop down
		.directive(
				'nonNulldrop',
				function() {
					return {
						require : 'ngModel',
						link : function(scope, element, attributes, control) {
							control.$validators.adult = function(modelValue,
									viewValue) {

								if (viewValue == undefined) {
									$(element).css({
										"border-color" : "red"
									})
								} else {
									$(element).css({
										"color" : "black",
										"border-color" : "green"
									})
									return true
								}
							};
						}
					};
				})

userManagementController.$inject = [ '$scope' ];
userManagementController.directive("datepicker", function () {
    return {
        restrict: "A",
        link: function (scope, el, attr) {
            el.datepicker({
                            dateFormat: 'dd/mm/yy'
                        });
        }
    };
})
var config = {
	headers : {
		'Accept' : 'application/json, */*'
	}
};

userManagementController
		.controller(
				'userManagement',
				function($scope, $http, $rootScope, $location, $window) {
					var myEl = angular.element(document
							.getElementsByName("myDataSid")[0]);
					var myEM = angular.element(document
							.getElementsByName("myDataEmail")[0]);
					$scope.sortBy = 'id';
					$scope.sortCondition = 'ASC';
					$scope.search = '';
					$scope.roleAdmin = [ {
						role : 'admin',
						roleDesc : 'Admin'
					}, {
						role : 'cs',
						roleDesc : 'Customer Service'
					},{
						role : 'su',
						roleDesc : 'Super Admin'
					} ];

					$scope.roleAdminEdit = [ {
						role : 'admin',
						roleDesc : 'Admin'
					}, {
						role : 'cs',
						roleDesc : 'Customer Service'
					}, 
					{
						role : 'su',
						roleDesc : 'Super Admin'
					},
					{
						role : 'nasabah',
						roleDesc : 'Nasabah'
					}
					 ];

					$scope.trueFalse = [ {
						id : true,
						value : 'Yes'
					}, {
						id : false,
						value : 'No'
					} ];
					$scope.descFunction = function(sortCondition, sortBy) {
						$scope.sortBy = sortBy;
						$scope.sortCondition = sortCondition;
						$scope.initializeData(1,'');
					}

					$scope.checkData = function(data) {
						$scope.selectedData = data;
						debugger;
					}

					$scope.searchFunction = function(text) {
						$scope.initializeData(1,text);
					}

					$scope.formMaster = {
						isEdit : false,
						isView : false,
						isWrong : false
					};

					// pass through untuk add
					$scope.softCode = function() {
						debugger;
						myEM.removeClass('right-answer');
						myEM.addClass('wrong-answer');
						myEl.removeClass('right-answer');
						myEl.addClass('wrong-answer');
						$scope.formMaster = {
								isEdit : false,
								isView : false
							};
						$scope.myData = {};
						$scope.myData.isLogin = $scope.trueFalse[1];
						$scope.myData.sidValidation = true;
						$scope.myData.sidValidationText = "Mohon Isi Data Terlebih Dahulu";
						$scope.myData.enabled = $scope.trueFalse[0];
						$scope.myData.locked = $scope.trueFalse[1];
						debugger;
					}

					$scope.checkValiditySid = function() {
						debugger;
						$http
								.get(
										"userMaintenanceWsCtl/check-validity/"
												+ $scope.myData.sid)
								.then(
										function(response) {
											debugger;
											if (response.data.isValid) {
												myEl.removeClass('wrong-answer');
												myEl.addClass('right-answer');
												$scope.reasonSid = undefined;
											} else {
												myEl.removeClass('right-answer');
												myEl.addClass('wrong-answer');
												$scope.reasonSid = response.data.reason;
											}
										},
										function error(response) {
											debugger;
											$scope.postResultMessage = "Error with status: "
													+ response.statusText;
											$scope.itsWinningSave.invalidModal = true;
											$scope.itsWinningSave.waitModal = false;
										});
					}
					
					$scope.checkValidityEmail = function() {
						debugger;
						$http
								.post(
										"userMaintenanceWsCtl/check-validity-email"
												, $scope.myData.email)
								.then(
										function(response) {
											debugger;
											if (response.data.isValid) {
												myEM.removeClass('wrong-answer');
												myEM.addClass('right-answer');
												$scope.reasonEmail = undefined;
											} else {
												myEM.removeClass('right-answer');
												myEM.addClass('wrong-answer');
												$scope.reasonEmail = response.data.reason;
											}
										},
										function error(response) {
											debugger;
											$scope.postResultMessage = "Error with status: "
													+ response.statusText;
											$scope.itsWinningSave.invalidModal = true;
											$scope.itsWinningSave.waitModal = false;
										});
					}

					$scope.trueFalseTable = function(str) {
						if (str == false) {
							return 'No';
						} else {
							return 'Yes';
						}
						debugger;
					}

					$scope.forcedClosed = function() {
						if ($scope.selectedData == undefined
								|| !$scope.selectedData.isSelected) {
							debugger;
							$scope.formMaster = {
								isEdit : false,
								isView : false
							};
							// $scope.formMaster.isWrong = true;
							alert('mohon pilih data terlebih dahulu');

						} else {
							$http
									.get(
											"userMaintenanceWsCtl/forcedClosed/"
													+ $scope.selectedData.userId)
									.then(
											function(response) {
												debugger;
												$scope.myData = {};
												$scope.initializeData(1,'');
											},
											function error(response) {
												debugger;
												$scope.postResultMessage = "Error with status: "
														+ response.statusText;
												$scope.itsWinningSave.invalidModal = true;
												$scope.itsWinningSave.waitModal = false;
											});
						}
						debugger;
					}

					$scope.seeMyData = function() {
						debugger;
						$scope.roleAdminEdit = [ {
							role : 'admin',
							roleDesc : 'Admin'
						}, {
							role : 'cs',
							roleDesc : 'Customer Service'
						}, 
						{
							role : 'su',
							roleDesc : 'Super Admin'
						},
						{
							role : 'nasabah',
							roleDesc : 'Nasabah'
						}
						 ];
						if ($scope.selectedData == undefined
								|| !$scope.selectedData.isSelected) {
							debugger;
							$scope.formMaster = {
								isEdit : false,
								isView : false
							};
							$scope.formMaster.isWrong = true;
							// alert('mohon pilih data terlebih dahulu');

						} else {
							$scope.myData = {};
							$scope.myData.sid = $scope.selectedData.userId;
							$scope.myData.namaLengkap = $scope.selectedData.namaLengkap;
							$scope.myData.email = $scope.selectedData.email;
							$scope.cannotBeChanged = false;
							if ($scope.selectedData.roles == 'Nasabah') {
								$scope.myData.roles = $scope.roleAdminEdit[3];
								$scope.cannotBeChanged = true;
							} else {
								for (var i = 0; i < $scope.roleAdminEdit.length; i++) {
									debugger;
									var roleDesc = $scope.roleAdminEdit[i].roleDesc
									if (roleDesc == $scope.selectedData.roles) {
										debugger;
										$scope.myData.roles = $scope.roleAdminEdit[i];
										$scope.cannotBeChanged = false;
										$scope.roleAdminEdit.splice(3, 1);
										break;
									}
								}
							}

							if ($scope.selectedData.locked) {
								$scope.myData.locked = $scope.trueFalse[0];
							} else {
								$scope.myData.locked = $scope.trueFalse[1];
							}

							if ($scope.selectedData.enabled) {
								$scope.myData.enabled = $scope.trueFalse[0];
							} else {
								$scope.myData.enabled = $scope.trueFalse[1];
							}

							if ($scope.selectedData.isLogin) {
								$scope.myData.isLogin = $scope.trueFalse[0];
							} else {
								$scope.myData.isLogin = $scope.trueFalse[1];
							}

							$scope.formMaster.isEdit = false;
							$scope.formMaster.isView = true;
							$scope.formMaster.isWrong = false;
							debugger;
						}
					}

					$scope.editMyData = function() {
						debugger;
						$scope.roleAdminEdit = [ {
							role : 'admin',
							roleDesc : 'Admin'
						}, {
							role : 'cs',
							roleDesc : 'Customer Service'
						}, 
						{
							role : 'su',
							roleDesc : 'Super Admin'
						},
						{
							role : 'nasabah',
							roleDesc : 'Nasabah'
						}
						 ];
						if ($scope.selectedData == undefined
								|| !$scope.selectedData.isSelected) {
							debugger;
							$scope.formMaster = {
								isEdit : false,
								isView : false
							};
							$scope.formMaster.isWrong = true;
							// alert('mohon pilih data terlebih dahulu');

						} else {
							$scope.myData = {};
							$scope.myData.sid = $scope.selectedData.userId;
							$scope.myData.namaLengkap = $scope.selectedData.namaLengkap;
							$scope.myData.email = $scope.selectedData.email;
							$scope.cannotBeChanged = false;
							if ($scope.selectedData.roles == 'Nasabah') {
								$scope.myData.roles = $scope.roleAdminEdit[3];
								$scope.cannotBeChanged = true;
							} else {
								for (var i = 0; i < $scope.roleAdminEdit.length; i++) {
									debugger;
									if ($scope.roleAdminEdit[i].roleDesc == $scope.selectedData.roles) {
										debugger;
										$scope.myData.roles = $scope.roleAdminEdit[i];
										$scope.cannotBeChanged = false;
										$scope.roleAdminEdit.splice(3, 1);
										break;
									}
								}
							}

							if ($scope.selectedData.locked) {
								$scope.myData.locked = $scope.trueFalse[0];
							} else {
								$scope.myData.locked = $scope.trueFalse[1];
							}

							if ($scope.selectedData.enabled) {
								$scope.myData.enabled = $scope.trueFalse[0];
							} else {
								$scope.myData.enabled = $scope.trueFalse[1];
							}

							if ($scope.selectedData.isLogin) {
								$scope.myData.isLogin = $scope.trueFalse[0];
							} else {
								$scope.myData.isLogin = $scope.trueFalse[1];
							}

							$scope.formMaster.isEdit = true;
							$scope.formMaster.isView = false;
							$scope.formMaster.isWrong = false;
							debugger;
						}
					}

					$scope.cancelViewEdit = function() {
						$scope.formMaster = {
							isEdit : false,
							isView : false
						};
						$scope.myData = {};
					}

					$scope.saveMyIdentity = function(data) {
						$scope.invalid = false;
						debugger;
						if($scope.reasonSid!=undefined){
							debugger;
							$scope.reason = $scope.reasonSid;
							$scope.invalid = true;
						}
						else if($scope.reasonEmail!=undefined){
							debugger;
							$scope.reason = $scope.reasonEmail;
							$scope.invalid = true;
						}
						else if(data.sid == '' || data.sid == undefined){
							debugger;
							$scope.reason = 'Mohon isi data sid terlebih dahulu';
							$scope.invalid = true;
						}
						
						else if(data.namaLengkap == undefined){
							debugger;
							$scope.reason = 'Mohon isi nama lengkap terlebih dahulu';
							$scope.invalid = true;
						}
						
						else if(data.email == undefined){
							debugger;
							$scope.reason = 'Mohon isi email terlebih dahulu';
							$scope.invalid = true;
						}
						else if(data.roles == undefined){
							debugger;
							$scope.reason = 'Mohon pilih role terlebih dahulu';
							$scope.invalid = true;
						}
						else{
							debugger;
						$scope.dataOlah = {};
						$scope.dataOlah.userSid = data.sid;
						$scope.dataOlah.userName = data.namaLengkap;
						$scope.dataOlah.email = data.email;
						$scope.dataOlah.isLogin = data.isLogin.id;
						$scope.dataOlah.roles = data.roles.role;
						$scope.dataOlah.enabled = data.enabled.id;
						$scope.dataOlah.locked = data.locked.id;
						$scope.dataOlah.rolesDesc = data.roles.roleDesc;
						$http
								.post("userMaintenanceWsCtl/add-data",
										$scope.dataOlah)
								.then(
										function(response) {
											debugger;
											if(response.data.valid){
											$scope.reason = "Data Berhasil Disimpan";
											$scope.myData = {};
											$scope.initializeData(1,'');
											}
											else{
												$scope.reason = response.data.reason;
												$scope.invalid = true;
											}
										},
										function error(response) {
											debugger;
											$scope.postResultMessage = "Error with status: "
													+ response.statusText;
											$scope.itsWinningSave.invalidModal = true;
											$scope.itsWinningSave.waitModal = false;
										});
						}
					}

					$scope.cancelSave = function() {
						$scope.myData = {};
					}

					$scope.initializeData = function(paging,searchEngine) {
						$http
								.post("userMaintenanceWsCtl/initialize-data/admin", {
									search : searchEngine,
									searchKolom : $scope.kolom,
									page : paging,
									sortBy : $scope.sortBy,
									sortCondition : $scope.sortCondition,
								})
								.then(
										function(response) {
											debugger;
											$scope.listSessions = response.data.content;
											$scope.pageNumber = response.data.pagination.page;
											$scope.totalPage = response.data.totalPage;
											$scope.total = response.data.totalData;
										},
										function error(response) {
											debugger;
											$scope.postResultMessage = "Error with status: "
													+ response.statusText;
											$scope.itsWinningSave.invalidModal = true;
											$scope.itsWinningSave.waitModal = false;
										});
					}

					$scope.initializeData(1,'');

				});
