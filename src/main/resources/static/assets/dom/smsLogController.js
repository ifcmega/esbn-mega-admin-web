var endpoints = location.origin;
var smsLogController = angular
		.module('smsLogController', [])
		.config(
				[
						'$httpProvider',
						function($httpProvider) {
							$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
						} ]).directive('format', ['$filter', function ($filter) {
						    return {
						        require: '?ngModel',
			 			        link: function (scope, elem, attrs, ctrl) {
						            if (!ctrl) return;


						            ctrl.$formatters.unshift(function (a) {
						                return $filter(attrs.format)(ctrl.$modelValue)
						            });


						            ctrl.$parsers.unshift(function (viewValue) {
						                var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
						                elem.val($filter(attrs.format)(plainNumber));
						                return plainNumber;
						            });
						        }
						    };
						}])
						.directive('nonNulldrop', function () {
	        return {
	            require: 'ngModel',
	            link: function (scope, element, attributes, control) {
	                control.$validators.adult = function (modelValue, viewValue) {
                         
                         if(viewValue==undefined){
                         $(element).css({"border-color" : "red"})
                         }
                         else{
                         $(element).css({"color":"black","border-color" : "green"}) 
                         return true
                         }
	                };
	            }
	        };
	    });

smsLogController.$inject = ['$scope'];
smsLogController.directive("datepicker", function () {
    return {
        restrict: "A",
        link: function (scope, el, attr) {
            el.datepicker({
                            dateFormat: 'dd/mm/yy'
                        });
        }
    };
})


var config = {
	headers : {
		'Accept' : 'application/json, */*'
	}
};

smsLogController.controller('smsLog', function($scope, $http,
		$rootScope, $location, $window) {
	$scope.sortBy = 'logTglWaktu';
	$scope.sortCondition = 'DESC';
	//$scope.search = '';
	$scope.search = '';
	$scope.status = '';
	$scope.searchKolom = '';
	
	$scope.searchMyData = function(page,inputan){
		debugger;
		$scope.search = inputan;
		if($scope.fromDate == undefined){
			$scope.fromDate = 'fromDate';
		}
		if($scope.toDate == undefined){
			$scope.toDate = 'toDate';
		}
		if($scope.sortBy == undefined){
			$scope.sortBy = 'logTglWaktu';	
		}
		if($scope.sortCondition == undefined){
			$scope.sortCondition = 'DESC';	
		}
		
		$http.post("smsLogWsCtl/get-sms-log",
				{
			           search : inputan,
			           searchKolom : $scope.kolom, 
			           page : page,
			           sortBy : $scope.sortBy,
			           sortCondition : $scope.sortCondition,
			           fromDate : $scope.fromDate,
			           status : $scope.status,
			           toDate : $scope.toDate,
				})
		.then(
				function(response) {
					
					if($scope.fromDate == 'fromDate'){
						$scope.fromDate = undefined;
					}
					if($scope.toDate == 'toDate'){
						$scope.toDate = undefined;
					}
					
					if (response.data.isError == true) {
						alert(response.data.reason);
					} else {
						$scope.listSmsLog = response.data.content;
						$scope.pageNumber = response.data.pagination.page;
						$scope.totalPage = response.data.totalPage;
						$scope.total = response.data.totalData;
					}
				},
				function error(response) {
					debugger;
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
					$scope.itsWinningSave.invalidModal = true;
					$scope.itsWinningSave.waitModal = false;
				});	
	}
	
$scope.searchMyFilterData = function(page){
		
		if($scope.fromDate == undefined){
			$scope.fromDate = 'fromDate';
		}
		if($scope.toDate == undefined){
			$scope.toDate = 'toDate';
		}
		if($scope.sortBy == undefined){
			$scope.sortBy = 'logTglWaktu';	
		}
		if($scope.sortCondition == undefined){
			$scope.sortCondition = 'DESC';	
		}
		
		$http.post("smsLogWsCtl/get-sms-log",
				{
			           search : $scope.search,
			           searchKolom : $scope.kolom, 
			           page : page,
			           sortBy : $scope.sortBy,
			           sortCondition : $scope.sortCondition,
			           fromDate : $scope.fromDate,
			           status : $scope.status,
			           toDate : $scope.toDate,
				})
		.then(
				function(response) {
					
					if($scope.fromDate == 'fromDate'){
						$scope.fromDate = undefined;
					}
					if($scope.toDate == 'toDate'){
						$scope.toDate = undefined;
					}
					
					if (response.data.isError == true) {
						alert(response.data.reason);
					} else {
						$scope.listSmsLog = response.data.content;
						$scope.pageNumber = response.data.pagination.page;
						$scope.totalPage = response.data.totalPage;
						$scope.total = response.data.totalData;
					}
				},
				function error(response) {
					debugger;
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
					$scope.itsWinningSave.invalidModal = true;
					$scope.itsWinningSave.waitModal = false;
				});	
	}
	
	
	$scope.searchMyData(0,'');
	
});

