var endpoints = location.origin;
var masterPictureController = angular
		.module('masterPictureController', [])
		.config(
				[
						'$httpProvider',
						function($httpProvider) {
							$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
						} ]).directive(
				'format',
				[
						'$filter',
						function($filter) {
							return {
								require : '?ngModel',
								link : function(scope, elem, attrs, ctrl) {
									if (!ctrl)
										return;

									ctrl.$formatters.unshift(function(a) {
										return $filter(attrs.format)(
												ctrl.$modelValue)
									});

									ctrl.$parsers.unshift(function(viewValue) {
										var plainNumber = viewValue.replace(
												/[^\d|\-+|\.+]/g, '');
										elem.val($filter(attrs.format)(
												plainNumber));
										return plainNumber;
									});
								}
							};
						} ]).directive(
				'nonNulldrop',
				function() {
					return {
						require : 'ngModel',
						link : function(scope, element, attributes, control) {
							control.$validators.adult = function(modelValue,
									viewValue) {

								if (viewValue == undefined) {
									$(element).css({
										"border-color" : "red"
									})
								} else {
									$(element).css({
										"color" : "black",
										"border-color" : "green"
									})
									return true
								}
							};
						}
					};
				});

masterPictureController.$inject = [ '$scope' ];
masterPictureController.directive("datepicker", function() {
	return {
		restrict : "A",
		link : function(scope, el, attr) {
			el.datepicker({
				dateFormat : 'dd/mm/yy'
			});
		}
	};
})

var config = {
	headers : {
		'Accept' : 'application/json, */*'
	}
};

masterPictureController.controller('masterPicture', function($scope, $http,
		$rootScope, $location, $window) {
	
	$scope.updateLang = function(number){
		$scope.number = number;
	}
	
	$scope.readFile = function(event) {
		debugger;
		$scope.input = event.target;
		
		var size = $scope.input.files[0].size;
		var nameUpload = $scope.input.files[0].name;
		var reader = new FileReader();
		
		
			reader.onload = function(e) {
				var nameUpload = $scope.input.files[0].name;
				dataURL = reader.result;
				$scope.dataUrl = {
						name : nameUpload,
						dataUri : reader.result,
						number : $scope.number
				}
				
				debugger;

				nameUpload = nameUpload.replace(/[^0-9a-z]/gi, '');
				$('#blah')
                .attr('src', e.target.result)
                .width(150)
                .height(200);
			}

		reader.readAsDataURL($scope.input.files[0]);
	}
	
	$scope.isActive = false;
	
	$scope.addFile = function(event) {
		debugger;
		$scope.input = event.target;
		var size = $scope.input.files[0].size;
		var nameUpload = $scope.input.files[0].name;
		var reader = new FileReader();
		
		
			reader.onload = function(e) {
				var nameUpload = $scope.input.files[0].name;
				dataURL = reader.result;
				
				$scope.dataUrl = {
						name : nameUpload,
						dataUri : reader.result,
						number : $scope.number
				}
				
				debugger;

				nameUpload = nameUpload.replace(/[^0-9a-z]/gi, '');
				$('#blah2')
                .attr('src', e.target.result)
                .width(150)
                .height(200);
			}

		reader.readAsDataURL($scope.input.files[0]);
	}
	
	$scope.dataUrl = undefined;
	$scope.itsLoading = {
		'waitModal' : true,
	    'invalidModal' : false,
	    'validModal' : false
	}

	$scope.SavePicture = function(){
		debugger;
		$scope.dataUrl;
		$scope.dataUrl.dataLink = $scope.dataLink;
		$http.post("shared/master-picture/uploadFile", $scope.dataUrl, config).then(function(response) {
			debugger;
			alert(response.data.value);
			if(response.data.valid){
				 $window.location.href = 'master-picture';
				//$scope.initCarousel();
			}
			else{
				$window.location.href = 'master-picture';
			}
	}, function errorCallback(response) {
		$window.alert("Pastikan extension File adalah .jpeg/jpg dan .png");
	});
	}
	
	$scope.addPicture = function(){
		debugger;
		$scope.dataUrl;
		$scope.dataUrl.dataLink = $scope.dataLink;
		$http.post("shared/master-picture/addFile", $scope.dataUrl, config).then(function(response) {
			debugger;
			alert(response.data.value);
			if(response.data.valid){
				 $window.location.href = 'master-picture';
				//$scope.initCarousel();
			}
			else{
				$window.location.href = 'master-picture';
			}
	}, function errorCallback(response) {
		$window.alert("Pastikan extension File adalah .jpeg/jpg dan .png");
	});
	}
	
	$scope.hapusLang = function(number){
		$scope.number = number;
	}

	$scope.deletePicture = function(){
		$http.get("shared/master-picture/deleteFile/"+$scope.number, config).then(function(response) {
			debugger;
			alert(response.data.value);
			if(response.data.valid){
				 $window.location.href = 'master-picture';
				//$scope.initCarousel();
			}
			else{
				$window.location.href = 'master-picture';
			}
	}, function errorCallback(response) {
		$window.alert("Pastikan extension File adalah .jpeg/jpg dan .png");
	});
	}
	
	$scope.cancelRegist = function(){
		$scope.dataUrl = undefined;
		$scope.dataLink = undefined;
		$scope.itsLoading = {
			'waitModal' : true,
		    'invalidModal' : false,
		    'validModal' : false
		}
	}
	
	$scope.initCarousel = function() {
		debugger;
		$http.get("shared/master-picture/get-all-master-carousel").then(
				function(response) {
					$scope.listPicture = response.data;
					debugger;
				},
				function error(response) {
					debugger;
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
					$scope.itsWinningSave.invalidModal = true;
					$scope.itsWinningSave.waitModal = false;
				});
	}

	$scope.initCarousel();
});
