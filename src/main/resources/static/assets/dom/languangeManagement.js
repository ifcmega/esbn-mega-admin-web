var endpoints = location.origin;
var languangeManagementController = angular
		.module('languangeManagementController',  ['ngSanitize'])
		.config(
				[
						'$httpProvider',
						function($httpProvider) {
							$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
						} ])
		.directive(
				'format',
				[
						'$filter',
						function($filter) {
							return {
								require : '?ngModel',
								link : function(scope, elem, attrs, ctrl) {
									if (!ctrl)
										return;
									ctrl.$formatters.unshift(function(a) {
										return $filter(attrs.format)(
												ctrl.$modelValue)
									});
									ctrl.$parsers.unshift(function(viewValue) {
										var plainNumber = viewValue.replace(
												/[^\d|\-+|\.+]/g, '');
										elem.val($filter(attrs.format)(
												plainNumber));
										return plainNumber;
									});
								}
							};
						} ])
		//validasi untuk input						
		.directive(
				'nonNull',
				function() {
					return {
						require : 'ngModel',
						link : function(scope, element, attributes, control) {
							control.$validators.adult = function(modelValue,
									viewValue) {

								if (viewValue == undefined) {
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								} else {
									$(element).css({
										"color" : "black",
										"border-color" : "green"
									})
									return true
								}
							};
						}
					};
				})
		//validasi untuk drop down
		.directive(
				'nonNulldrop',
				function() {
					return {
						require : 'ngModel',
						link : function(scope, element, attributes, control) {
							control.$validators.adult = function(modelValue,
									viewValue) {

								if (viewValue == undefined) {
									$(element).css({
										"border-color" : "red"
									})
								} else {
									$(element).css({
										"color" : "black",
										"border-color" : "green"
									})
									return true
								}
							};
						}
					};
				})

		.directive(
				'nominalValidation',
				function() {
					return {
						require : 'ngModel',
						link : function(scope, element, attributes, control) {
							control.$validators.adult = function(modelValue,
									viewValue) {
								var minPemesanan = parseInt(document
										.getElementsByName("minPemesanan")[0].value
										.replace(/,/g, ''));
								var maxPemesanan = parseInt(document
										.getElementsByName("maxPemesanan")[0].value
										.replace(/,/g, ''));
								var kelipatan = parseInt(document
										.getElementsByName("kelipatan")[0].value
										.replace(/,/g, ''));

								if (modelValue == undefined)
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								else if (isNaN(minPemesanan)
										&& isNaN(maxPemesanan))
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								else if (parseInt(modelValue) > maxPemesanan
										|| parseInt(modelValue) < minPemesanan)
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								else if (modelValue % parseInt(kelipatan) != 0)
									$(element).css({
										"color" : "red",
										"border-color" : "red"
									})
								else {
									$(element).css({
										"color" : "black",
										"border-color" : "green"
									})
									return true;
								}
							};
						}
					};
				});

languangeManagementController.$inject = [ '$scope' ];

var config = {
	headers : {
		'Accept' : 'application/json, */*'
	}
};

languangeManagementController.controller('languangeManagement', function($scope,
		$http, $rootScope, $location, $window) {
	
	$scope.kolom = {};
	
	$scope.okUpdateData = function(){
		$http.post("maintenaceLanguanges/update-my-languange",
				$scope.listUpdated)
		.then(
				function(response) {
				debugger;
				$scope.pageFunction(1);
				},
				function error(response) {
					debugger;
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
					$scope.itsWinningSave.invalidModal = true;
					$scope.itsWinningSave.waitModal = false;
				});	
	}
	
	$scope.updateLang = function(list){
		$scope.listUpdated = list;
		
		debugger;
	}
	
	$scope.filterFunction = function(pageNumber){
		if($scope.kolom.type == ''){
			alert('pastikan untuk memilih drop list Kategori');
		}
		
		else if($scope.kolom.lang == ''){
			alert('pastikan untuk memilih drop list Bahasa');
		}
		
		else{
			$scope.pageFunction(pageNumber);
		}
		
	}
	
	$scope.pageFunction = function(pageNumber){
		if($scope.sort==undefined){
			$scope.sort = 'refferenceId,asc';
		}
		
		if($scope.kolom.type == undefined){
			$scope.kolom.type = '';
		}
		
		if($scope.kolom.lang == undefined){
			$scope.kolom.lang = '';
		}
		
		$http.post("maintenaceLanguanges/retrieve-all-languange?page="+pageNumber+"&sort="+$scope.sort,
				{
			          type : $scope.kolom.type,
			          code : 'DiscAndGuide',
			          languange : $scope.kolom.lang
				})
		.then(
				function(response) {
				debugger;
				$scope.listLanguange = response.data.content;
				$scope.pageNumber = response.data.pagination.page;
				$scope.totalPage = response.data.totalPage;
				$scope.total = response.data.totalData;
				},
				function error(response) {
					debugger;
					$scope.postResultMessage = "Error with status: "
							+ response.statusText;
					$scope.itsWinningSave.invalidModal = true;
					$scope.itsWinningSave.waitModal = false;
				});	
	}
	
	$scope.pageFunction(1);
	
});
