package com.bank.mega;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.Map.Entry;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.bank.mega.bean.admin.InjectorUserVariable;
import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.encryptor.SecurityData;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.security.ParamQueryCustomLib;
import com.bank.mega.security.PrincipalSecurityData;
import com.bank.mega.security.WsResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.google.gson.Gson;

@Component
public class UltimateBase {
   @Value("${wms.parental.wrapper}")
   protected String wmsParentalWrapper;
   
   @Value("${wms.parental.user-uda}")
   protected String USER_UDA;
   
   @Value("${wms.parental.user-password}")
   protected String PASSWORD_UDA;
   
   @Value("${esbn.email-name}")
   protected String esbnEmailName;
   
   @Value("${esbn.email-password}")
   protected String esbnEmailPassword;
   
   @Value("${wms.parental.grant-type}")
   protected String GRANT_TYPE;
   
   @Value("${esbn-web.parental}")
   protected String ESBN_WEB_PARENTAL;
   
   protected int COUNTER_PAGING = 20;
   
   protected final String CONTENT_FORMAT_DATE = "dd/MM/yyyy hh:mm:ss";
   protected final String ADDITION_USER = "esbn-web";
   protected final String ADDITION_PASS = "TRUST-ME!CLIENT";
   protected final static String SUFFICIENT_SAVE = "1";
   protected final static String INSUFFICIENT_SAVE = "0";
   protected final static String STREAM_ESBN = "";
   protected final static String STREAM_WMS = "/registeration";
   
   @Autowired
   private com.bank.mega.service.auth.UserAuthenticationCustomLogin userAuthenticationCustomLogin;
   
   
   
   public static void limitedExceededSession(String username,SessionRegistry sessionRegistry) {
   	
       for (Object principal : sessionRegistry.getAllPrincipals()) {
       	System.err.println("ses  " + new Gson().toJson(principal));
       	SessionInformation information = (SessionInformation) sessionRegistry.getAllSessions(principal, true);
       	information.expireNow();
       }
   }
   
   protected TokenWrapper getMyToken(String userName, String userPassword,String access) {
		InjectorUserVariable inject = new InjectorUserVariable();
		inject.setAccessType(access);
		inject.setPassword(userPassword);
		inject.setUser(userName);
		Map<String, String> header = new HashMap<>();
		header.put("secret-method","password");
		HttpRestResponse httpRestResponse = new HttpRestResponse();
		try {
			httpRestResponse = wsBody(wmsParentalWrapper+ "/shared/oauth/token/v2/password-granter", 
					new SecurityData().encrypt(new Gson().toJson(inject)), HttpMethod.POST,header);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Map<String, Object> mapToken = new HashMap<>();
		mapToken = mapResultApi(httpRestResponse.getBody());
		TokenWrapper tokenWrapper = new TokenWrapper();
		try {
			tokenWrapper = mapperHashmapToSingleDto(mapToken, TokenWrapper.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tokenWrapper;
  } 
  
  protected PrincipalSecurityData getCredentialPrincipal(Authentication authentication) {
	   String auth = new Gson().toJson(authentication.getPrincipal());
	    PrincipalSecurityData securityData = new PrincipalSecurityData();
		try {
			securityData = mapperJsonToSingleDto(auth, PrincipalSecurityData.class);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return securityData;
  }
  
  protected TokenWrapper getMyToken(Authentication authentication) {
	    String auth = new Gson().toJson(authentication.getPrincipal());
	    PrincipalSecurityData securityData = new PrincipalSecurityData();
		try {
			securityData = mapperJsonToSingleDto(auth, PrincipalSecurityData.class);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		InjectorUserVariable inject = new InjectorUserVariable();
		inject.setAccessType("admin");
		inject.setPassword(securityData.getPassword());
		inject.setUser(securityData.getUserName());
		Map<String, String> header = new HashMap<>();
		header.put("secret-method","password");
		HttpRestResponse httpRestResponse = new HttpRestResponse();
		try {
			httpRestResponse = wsBody(wmsParentalWrapper+ "/shared/oauth/token/v2/password-granter", 
					new SecurityData().encrypt(new Gson().toJson(inject)), HttpMethod.POST,header);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Map<String, Object> mapToken = new HashMap<>();
		mapToken = mapResultApi(httpRestResponse.getBody());
		TokenWrapper tokenWrapper = new TokenWrapper();
		try {
			tokenWrapper = mapperHashmapToSingleDto(mapToken, TokenWrapper.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tokenWrapper;
}  
  
   protected static String parseDateToString(Date date, String format) {
		String dateFor = null;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
	    dateFor = sdf.format(date);
		return dateFor;
	}
   
   protected static String parseDateToStringV2(Date date) {
		String dateFor = null;
		//DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		SimpleDateFormat sdfr = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
	    //dateFor = dateFormat.format(date);
		try{
			dateFor = sdfr.format( date );
		   }catch (Exception ex ){
			System.out.println(ex);
		   }
		return dateFor;
	}
   
   
   protected static String parseStringToOtherStringFormat(String dateStr, String oldFormat, String newFormat) throws ParseException {
	   SimpleDateFormat formatter = new SimpleDateFormat(oldFormat);
	   Date date = formatter.parse(dateStr);
	   String dateInNewFormat = parseDateToString(date, newFormat);
	   return dateInNewFormat;
   }
   
   protected static String parseStringToOtherStringFormatV2(
		   String dateStr, String oldFormat, String newFormat, String otherComposition)  {
	   SimpleDateFormat formatter = new SimpleDateFormat(oldFormat);
	   Date date = null;
	try {
		date = formatter.parse(dateStr);
	} catch (ParseException e) {
		try {
			date = formatter.parse(otherComposition);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	   String dateInNewFormat = parseDateToString(date, newFormat);
	   return dateInNewFormat;
   }
   
   public  boolean sendConfirmationUserEmail(String to, String name, String sid, String password) {
	   System.out.println(" start send to " + to);
	   String bodyEmail = "<div> "
   			+ " <p>Kepada Yth, Bapak/Ibu "+name+" </p>"
   			+ " </p></p>"
   			+ " <p>Terima kasih atas kepercayaan Anda Untuk bergabung dengan layanan SBN Ritel Online Bank Mega."
   			+ " Berikut adalah konfirmasi untuk melakukan aktivitas akun Anda. "
   			+ " <p>"
   			+ " <b><small>"
   			+ " Tanggal, Waktu :  " + parseDateToString(new Date(), "dd MMM yyyy, HH:mm")
   			+ " </small></b> </p>"
   			+ " <p><b><small>"
   			+ " Username :  " + sid
   			+ " </small></b>  </p>"
   			+ " <p><b><small>"
   			+ " Password : " + password
   			+ " </small></b> </p>"
   			+ " </p> "
   			+ " <p>"
   			+ " Silahkan melakukan aktivasi akun Anda dengan cara melakukan login pada SBN Ritel Online Bank Mega "
   			+ " menggunakan username dan password di atas. Untuk keamanan Anda segera ubah Password Anda setelah melakukan login pertama kali. "
   			+ " </p>"
   			+ " <p>Mohon tidak memberikan Username dan Password Anda kepihak manapun. Segala risiko yang timbul akibat penyalahgunaan"
   			+ " Username dan Password bukan merupakan tanggung jawab PT Bank Mega, Tbk.</p>"
   			+ " </p> "
   			+ " <p>Terima kasih</p>"
   			+ " PT Bank Mega, Tbk"
   			+ " </div>";
       return sendMailTest(to, "Konfirmasi Password Login E-SBN Bank Mega", 
   			bodyEmail, 
   			null, null);
   }
   
   public  boolean sendMailTest(String to, String subject, String body, String attachment, Map<String, String> mapInlineImages){

	   String username = esbnEmailName;
		//final String username_from = "digirec.ifwd@fwd.co.id";
		String password = esbnEmailPassword;
        System.err.println("pass : " + password + " user : " + username);
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.setProperty("mail.transport.protocol", "smtp"); 
		props.setProperty("mail.smtp.quitwait", "false"); 
		props.setProperty("mail.host", "smtp.bankmega.com"); 
		props.put("mail.smtp.socketFactory.fallback", "false"); 
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.bankmega.com");
		props.put("mail.smtp.port", "465");
		props.put("mail.smtp.socketFactory.class", 
	                "javax.net.ssl.SSLSocketFactory"); 

		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });

		try {

//		    message.setFrom(new InternetAddress(username)); 
//		      
//		    message.addRecipient(Message.RecipientType.TO,  
//		                          new InternetAddress(to)); 
//		    message.setSubject("Verifikasi Informasi User"); 
//		    message.setText("Hi, I'm Sending From Java"); 
//		  
//		    // Send message 
//		    Transport.send(message); 
//		    System.out.println("Yo it has been sent.."); 
			
			// creates a new e-mail message
			MimeMessage msg = new MimeMessage(session);
	 
	        msg.setFrom(new InternetAddress(username));
	       // msg.setFrom(new InternetAddress(username_from));
	        InternetAddress[] toAddresses = { new InternetAddress(to) };
	        msg.setRecipients(Message.RecipientType.TO, toAddresses);
	        msg.setSubject(subject);
	        msg.setSentDate(new Date());
	 
	        // creates message part
	        MimeBodyPart messageBodyPart = new MimeBodyPart();
	        messageBodyPart.setContent(body, "text/html");
	 
	        // creates multi-part
	        Multipart multipart = new MimeMultipart();
	        multipart.addBodyPart(messageBodyPart);
	 
	        // adds inline image attachments
	        if (mapInlineImages != null && mapInlineImages.size() > 0) {
	            Set<String> setImageID = mapInlineImages.keySet();
	             
	            for (String contentId : setImageID) {
	                MimeBodyPart imagePart = new MimeBodyPart();
	                imagePart.setHeader("Content-ID", "<" + contentId + ">");
	                imagePart.setDisposition(MimeBodyPart.INLINE);
	                 
	                String imageFilePath = mapInlineImages.get(contentId);
	                try {
	                    imagePart.attachFile(imageFilePath);
	                } catch (IOException ex) {
	                    ex.printStackTrace();
	                }
	 
	                multipart.addBodyPart(imagePart);
	            }
	        }
	 
	        msg.setContent(multipart);
	        System.err.println("masuk kesini");
	        
	        
	        Transport.send(msg);

			System.out.println("Done");
            return true;
		} catch (MessagingException e) {
			e.printStackTrace();
			return false;
		}
	}
   
   protected String randomString(int leng) {
	    int leftLimit = 97; // letter 'a'
	    int rightLimit = 122; // letter 'z'
	    int targetStringLength = leng;
	    Random random = new Random();
	    StringBuilder buffer = new StringBuilder(targetStringLength);
	    for (int i = 0; i < targetStringLength; i++) {
	        int randomLimitedInt = leftLimit + (int) 
	          (random.nextFloat() * (rightLimit - leftLimit + 1));
	        buffer.append((char) randomLimitedInt);
	    }
	    String generatedString = buffer.toString();
	 
	    return generatedString;
   }
   
    protected HttpRestResponse wsBodyWms(String secretUrl, String secretMethod,
    		Object body, Map<String, String> headerMap,
			ParamQueryCustomLib... paramQuery) {
    	if(body==null) {
    		body = new HashMap<>();
    	}
    	if(headerMap==null) {
    		headerMap = new HashMap<>();
    	}
		headerMap.put("secret-url", secretUrl);
		headerMap.put("secret-method", secretMethod);
		HttpRestResponse result = wsBody(wmsParentalWrapper+STREAM_WMS+"/WMS-URL", body, 
				HttpMethod.POST, headerMap);
		return result;
    }
    
    protected HttpRestResponse wsBodyWms(String secretUrl, String secretMethod,
    		Object body, Map<String, String> headerMap,String authorization,
			ParamQueryCustomLib... paramQuery) {
    	if(body==null) {
    		body = new HashMap<>();
    	}
    	if(headerMap==null) {
    		headerMap = new HashMap<>();
    	}
    	headerMap.put("Authorization", authorization);
		headerMap.put("secret-url", secretUrl);
		headerMap.put("secret-method", secretMethod);
		System.out.println("my header : " + new Gson().toJson(headerMap));
		HttpRestResponse result = wsBody(wmsParentalWrapper+STREAM_WMS+"/WMS-URL", body, 
				HttpMethod.POST, headerMap);
		return result;
    }

    protected HttpRestResponse wsBodyEsbnWithoutBody(String secretUrl, String secretMethod,
    	     Map<String, String> headerMap,String authorization,
			ParamQueryCustomLib... paramQuery) {

    	if(headerMap==null) {
    		headerMap = new HashMap<>();
    	}
    	headerMap.put("Authorization", authorization);
		headerMap.put("secret-url", secretUrl);
		headerMap.put("secret-method", secretMethod);
		System.out.println("my header : " + new Gson().toJson(headerMap));
		HttpRestResponse result = wsBody(wmsParentalWrapper+STREAM_ESBN+"/esbn-connector/call-url/no-body", null, 
				HttpMethod.GET, headerMap);
		return result;
    }
    
    protected HttpRestResponse wsBodyEsbnWithBody(String secretUrl, String secretMethod,
   	     Object body,Map<String, String> headerMap,String authorization,
			ParamQueryCustomLib... paramQuery) {

   	if(headerMap==null) {
   		headerMap = new HashMap<>();
   	}
   	headerMap.put("Authorization", authorization);
		headerMap.put("secret-url", secretUrl);
		headerMap.put("secret-method", secretMethod);
		System.out.println("my header : " + new Gson().toJson(headerMap));
		HttpRestResponse result = wsBody(wmsParentalWrapper+STREAM_ESBN+"/esbn-connector/call-url/with-body", body, 
				HttpMethod.POST, headerMap);
		return result;
   }
    
    protected WsResponse getResultWsWms(String secretUrl, String secretMethod,
    		Object body, Map<String, String> headerMap,
			ParamQueryCustomLib... paramQuery) {
    	WsResponse wsResponse = new WsResponse();
    	if(body==null) {
    		body = new HashMap<>();
    	}
		HttpRestResponse httpRestResponse = wsBodyWms(secretUrl,secretMethod,
	    		body, headerMap,paramQuery);

		switch (httpRestResponse.getStatus()) {
		case OK:
			ObjectMapper om = new ObjectMapper();
			om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			try {
				wsResponse = om.readValue(httpRestResponse.getBody(), WsResponse.class);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return wsResponse;
		case NOT_ACCEPTABLE:
			return null;
		case INTERNAL_SERVER_ERROR:
			return null;
		default:
			return null;
		}
    }
    
	protected WsResponse getResultWs(String url, Object body, HttpMethod method, Map<String, String> headerMap,
			ParamQueryCustomLib... paramQuery) {
		WsResponse wsResponse = new WsResponse();
		HttpRestResponse httpRestResponse = wsBody(url, body, method, headerMap, paramQuery);

		switch (httpRestResponse.getStatus()) {
		case OK:
			ObjectMapper om = new ObjectMapper();
			om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			try {
				wsResponse = om.readValue(httpRestResponse.getBody(), WsResponse.class);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return wsResponse;
		case NOT_ACCEPTABLE:
			return null;
		case INTERNAL_SERVER_ERROR:
			return null;
		default:
			return null;
		}
	}

	protected HttpRestResponse WsBodyWithSecurity(String url, Object body, HttpMethod method, String security ,Map<String, String> headerMap,
			ParamQueryCustomLib... paramQuery) {
		if (headerMap==null) {
			headerMap = new HashMap<>();
		}
		
		headerMap.put("Authorization", security);
		return wsBody(url, body, method, headerMap, paramQuery);
	}
    
    @SuppressWarnings("rawtypes")
	protected HttpRestResponse wsBody(String url, Object body, HttpMethod method, Map<String, String> headerMap,
			ParamQueryCustomLib... paramQuery) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		if (headerMap != null) {
			for (Entry<String, String> hm : headerMap.entrySet()) {
				headers.add(hm.getKey(), hm.getValue());
			}
		}
		StringBuilder paramBuilder = new StringBuilder();
		if (paramQuery != null) {
			if (paramQuery.length != 0) {
				paramBuilder.append("?");
				for (int i = 0; i < paramQuery.length; i++) {
					paramBuilder.append(paramQuery[i].getKey().concat("=".concat(paramQuery[i].getValue())));
					if (i < paramQuery.length - 1) {
						paramBuilder.append("&");
					}
				}
			}
		}
		System.err.println("body : " + new Gson().toJson(body));
		System.err.println("header : " + new Gson().toJson(headers));

		HttpEntity httpEntity = new HttpEntity(body, headers);
		RestTemplate restTemplate = new RestTemplate();
		System.err.println("url yang diberikan : " + url.concat(paramBuilder.toString()));

		try {
			ResponseEntity<String> responseEntity = restTemplate.exchange(url.concat(paramBuilder.toString()), method,
					httpEntity, String.class);
			System.err.println("status : " + responseEntity.getStatusCode());
			System.err.println("result api : " + responseEntity.getBody());
			return new HttpRestResponse(responseEntity.getStatusCode(), responseEntity.getBody());
		} catch (Exception exp) {
			if (exp.getMessage().contains("400")) {
				System.err.println("error 400");
				return new HttpRestResponse(HttpStatus.BAD_REQUEST, "User atau Password Salah");
			} else if (exp.getMessage().contains("Connection refused")) {
				System.err.println("Connection refused");
				return new HttpRestResponse(HttpStatus.INTERNAL_SERVER_ERROR,
						"Tidak dapat berkomunikasi dengan service");
			} else {
				System.err.println("exp said: " + exp.getMessage());
				System.err.println("error unidentified");
				return new HttpRestResponse(HttpStatus.NOT_EXTENDED, "Cannot Identified Error Record");
			}
		}
	}
   
   
	protected String restingToken(String userName, String userPassword) {
		Map<String, Object> mapToken = new HashMap<>();
		Map<String, String> header = new HashMap<>();
//		header.put("Authorization",
//				"Basic ".concat("bXktdHJ1c3RlZC1jbGllbnQ6c2VjcmV0"));
		header.put("Authorization",
				"Basic ".concat(Base64.getEncoder().encodeToString((USER_UDA + ":" + PASSWORD_UDA).getBytes())));
		HttpRestResponse httpRestResponse = wsBody(wmsParentalWrapper + "/oauth/token", null, HttpMethod.POST, header,
				new ParamQueryCustomLib("grant_type", GRANT_TYPE), new ParamQueryCustomLib("username", userName),
				new ParamQueryCustomLib("password", userPassword));
		switch (httpRestResponse.getStatus()) {
		case OK:

			mapToken = mapResultApi(httpRestResponse.getBody());
			if (mapToken != null && mapToken.get("access_token") != null) {
				return (String) mapToken.get("access_token");
			}

			break;

		case NOT_ACCEPTABLE:
			return null;
		case INTERNAL_SERVER_ERROR:
			return null;
		default:
			return null;
		}

		return null;
	}

	private Map<String, Object> mapResultApi(String result) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		Map<String, Object> finalMap = new HashMap<>();
		try {
			finalMap = mapper.readValue(result, new TypeReference<HashMap<String, Object>>() {
			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return finalMap;
	}

	
	protected Map<String, Object> mapperJsonToHashMap(String result) {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		Map<String, Object> finalMap = new HashMap<>();
		try {
			finalMap = mapper.readValue(result, new TypeReference<HashMap<String, Object>>() {
			});
		} catch (Exception e) {
			e.printStackTrace();
			finalMap.put("error_method", e.getMessage());
		}
		return finalMap;
	}
	
	protected String setObjectToString(Object object) {
		return new Gson().toJson(object);
	}

	protected <T> T mapperJsonToSingleDto(String json, Class<T> clazz) throws Exception {
		ObjectMapper om = new ObjectMapper();
		om.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		om.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, false);
		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		om.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		return om.readValue(json, clazz);
	}
	
	protected <T> T mapperHashmapToSingleDto(Map<String, Object> json, Class<T> clazz) throws Exception {
		ObjectMapper om = new ObjectMapper();
		om.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		om.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, false);
		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		om.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		return om.convertValue(json, clazz);	
		}

	protected <T> List<T> mapperJsonToListDto(String json, Class<T> clazz) throws Exception {
		ObjectMapper om = new ObjectMapper();
		om.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
		om.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, false);
		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		om.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		TypeFactory t = TypeFactory.defaultInstance();
		List<T> list = om.readValue(json, t.constructCollectionType(ArrayList.class, clazz));
		return list;
	}
	
}
