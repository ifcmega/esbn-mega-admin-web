package com.bank.mega.bean.log;

import java.util.Date;

public class SmsLogBean {
	private Long logId;
	private Date logTglWaktu;
	private String logTujuan;
	private String logSubject;
	private String logUser;
	private String logResend;
	private String logPesan;
	private String logTglWaktuString;
	private String logStatus;
 
	public String getLogPesan() {
		return logPesan;
	}

	public void setLogPesan(String logPesan) {
		this.logPesan = logPesan;
	}

	public Long getLogId() {
		return logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public Date getLogTglWaktu() {
		return logTglWaktu;
	}

	public void setLogTglWaktu(Date logTglWaktu) {
		this.logTglWaktu = logTglWaktu;
	}

	public String getLogTujuan() {
		return logTujuan;
	}

	public void setLogTujuan(String logTujuan) {
		this.logTujuan = logTujuan;
	}

	public String getLogSubject() {
		return logSubject;
	}

	public void setLogSubject(String logSubject) {
		this.logSubject = logSubject;
	}

	public String getLogUser() {
		return logUser;
	}

	public void setLogUser(String logUser) {
		this.logUser = logUser;
	}

	public String getLogResend() {
		return logResend;
	}

	public void setLogResend(String logResend) {
		this.logResend = logResend;
	}

	public String getLogTglWaktuString() {
		return logTglWaktuString;
	}

	public void setLogTglWaktuString(String logTglWaktuString) {
		this.logTglWaktuString = logTglWaktuString;
	}

	public String getLogStatus() {
		return logStatus;
	}

	public void setLogStatus(String logStatus) {
		this.logStatus = logStatus;
	}

}
