package com.bank.mega.bean.log;

import java.util.List;

import com.bank.mega.bean.admin.PageRequestBean;

public class SmsLogBeanListPaging {
	private List<SmsLogBean> content;
	private PageRequestBean pagination;
	private String query;
	private int totalData;
	private int totalPage;
	private Boolean isError;
	private String reason;

	public List<SmsLogBean> getContent() {
		return content;
	}

	public void setContent(List<SmsLogBean> content) {
		this.content = content;
	}

	public PageRequestBean getPagination() {
		return pagination;
	}

	public void setPagination(PageRequestBean pagination) {
		this.pagination = pagination;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public int getTotalData() {
		return totalData;
	}

	public void setTotalData(int totalData) {
		this.totalData = totalData;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public Boolean getIsError() {
		return isError;
	}

	public void setIsError(Boolean isError) {
		this.isError = isError;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
