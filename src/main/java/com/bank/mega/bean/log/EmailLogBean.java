package com.bank.mega.bean.log;

import java.util.Date;

public class EmailLogBean {
	private Long logId;
	private Date logTglWaktu;
	private String logTujuan;
	private String logCc;
	private String logSubject;
	private String logStatus;
	private String logUser;
	private String logResend;
	private String logTglWaktuString;

	 
	public String getLogTglWaktuString() {
		return logTglWaktuString;
	}

	public void setLogTglWaktuString(String logTglWaktuString) {
		this.logTglWaktuString = logTglWaktuString;
	}

	public Long getLogId() {
		return logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public Date getLogTglWaktu() {
		return logTglWaktu;
	}

	public void setLogTglWaktu(Date logTglWaktu) {
		this.logTglWaktu = logTglWaktu;
	}

	public String getLogTujuan() {
		return logTujuan;
	}

	public void setLogTujuan(String logTujuan) {
		this.logTujuan = logTujuan;
	}

	public String getLogCc() {
		return logCc;
	}

	public void setLogCc(String logCc) {
		this.logCc = logCc;
	}

	public String getLogSubject() {
		return logSubject;
	}

	public void setLogSubject(String logSubject) {
		this.logSubject = logSubject;
	}

	public String getLogStatus() {
		return logStatus;
	}

	public void setLogStatus(String logStatus) {
		this.logStatus = logStatus;
	}

	public String getLogUser() {
		return logUser;
	}

	public void setLogUser(String logUser) {
		this.logUser = logUser;
	}

	public String getLogResend() {
		return logResend;
	}

	public void setLogResend(String logResend) {
		this.logResend = logResend;
	}
}
