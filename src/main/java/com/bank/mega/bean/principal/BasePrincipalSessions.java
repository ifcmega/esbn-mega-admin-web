package com.bank.mega.bean.principal;

import java.util.Comparator;

public class BasePrincipalSessions  implements Comparable<ComparablePrincipalSessions> {
     private String lastRequest;
     private CustomUserServiceBean principal;
     private String sessionId;
     private boolean expired;
     private String lastRequestOnFormat;
	 private String firstLoginInFormat;
    
    
	
	public String getFirstLoginInFormat() {
		return firstLoginInFormat;
	}
	public void setFirstLoginInFormat(String firstLoginInFormat) {
		this.firstLoginInFormat = firstLoginInFormat;
	}
	public String getLastRequestOnFormat() {
		return lastRequestOnFormat;
	}
	public void setLastRequestOnFormat(String lastRequestOnFormat) {
		this.lastRequestOnFormat = lastRequestOnFormat;
	}
	public String getLastRequest() {
		return lastRequest;
	}
	public void setLastRequest(String lastRequest) {
		this.lastRequest = lastRequest;
	}
	public CustomUserServiceBean getPrincipal() {
		return principal;
	}
	public void setPrincipal(CustomUserServiceBean principal) {
		this.principal = principal;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public boolean isExpired() {
		return expired;
	}
	public void setExpired(boolean expired) {
		this.expired = expired;
	}
	
	@Override
	public int compareTo(ComparablePrincipalSessions o) {
		// TODO Auto-generated method stub
		return 0;
	}
     

	public static Comparator<BasePrincipalSessions> UserIdComparatorAsc =
			new Comparator<BasePrincipalSessions>() {
		public int compare(BasePrincipalSessions comp1, BasePrincipalSessions comp2) {
			String fruitName1 = comp1.principal.getUserName().toUpperCase();
			String fruitName2 = comp2.principal.getUserName().toUpperCase();
			return fruitName1.compareTo(fruitName2);
		}

	};
	
	public static Comparator<BasePrincipalSessions> UserIdComparatorDesc =
			new Comparator<BasePrincipalSessions>() {
		public int compare(BasePrincipalSessions comp1, BasePrincipalSessions comp2) {
			String fruitName1 = comp1.principal.getUserName().toUpperCase();
			String fruitName2 = comp2.principal.getUserName().toUpperCase();		
            return fruitName2.compareTo(fruitName1);
		}

	};
	
	public static Comparator<BasePrincipalSessions> TokenAsc =
			new Comparator<BasePrincipalSessions>() {
		public int compare(BasePrincipalSessions comp1, BasePrincipalSessions comp2) {
			String fruitName1 = comp1.getSessionId().toUpperCase();
			String fruitName2 = comp2.getSessionId().toUpperCase();	
			return fruitName1.compareTo(fruitName2);
            //return fruitName2.compareTo(fruitName1);
		}
	};
	
	public static Comparator<BasePrincipalSessions> TokenDesc =
			new Comparator<BasePrincipalSessions>() {
		public int compare(BasePrincipalSessions comp1, BasePrincipalSessions comp2) {
			String fruitName1 = comp1.getSessionId().toUpperCase();
			String fruitName2 = comp2.getSessionId().toUpperCase();	
			//return fruitName1.compareTo(fruitName2);
            return fruitName2.compareTo(fruitName1);
		}
	};
	
	
//	public static Comparator<BasePrincipalSessions> HostAsc =
//			new Comparator<BasePrincipalSessions>() {
//		public int compare(BasePrincipalSessions comp1, BasePrincipalSessions comp2) {
//			String fruitName1 = comp1.getSessionId().toUpperCase();
//			String fruitName2 = comp2.getSessionId().toUpperCase();	
//			//return fruitName1.compareTo(fruitName2);
//            return fruitName2.compareTo(fruitName1);
//		}
//	};
	
	
	public static Comparator<BasePrincipalSessions> WaktuLoginAsc =
			new Comparator<BasePrincipalSessions>() {
		public int compare(BasePrincipalSessions comp1, BasePrincipalSessions comp2) {
			String fruitName1 = comp1.getPrincipal().getFirstLogin().toUpperCase();
			String fruitName2 = comp2.getPrincipal().getFirstLogin().toUpperCase();
			return fruitName1.compareTo(fruitName2);
            //return fruitName2.compareTo(fruitName1);
		}
	};
	
	public static Comparator<BasePrincipalSessions> WaktuLoginDesc =
			new Comparator<BasePrincipalSessions>() {
		public int compare(BasePrincipalSessions comp1, BasePrincipalSessions comp2) {
			String fruitName1 = comp1.getPrincipal().getFirstLogin().toUpperCase();
			String fruitName2 = comp2.getPrincipal().getFirstLogin().toUpperCase();
			//return fruitName1.compareTo(fruitName2);
            return fruitName2.compareTo(fruitName1);
		}
	};

	public static Comparator<BasePrincipalSessions> AktivitasTerakhirAsc =
			new Comparator<BasePrincipalSessions>() {
		public int compare(BasePrincipalSessions comp1, BasePrincipalSessions comp2) {
			String fruitName1 = comp1.lastRequest.toUpperCase();
			String fruitName2 = comp2.lastRequest.toUpperCase();
			return fruitName1.compareTo(fruitName2);
            //return fruitName2.compareTo(fruitName1);
		}
	};
	
	public static Comparator<BasePrincipalSessions> AktivitasTerakhirDesc =
			new Comparator<BasePrincipalSessions>() {
		public int compare(BasePrincipalSessions comp1, BasePrincipalSessions comp2) {
			String fruitName1 = comp1.lastRequest.toUpperCase();
			String fruitName2 = comp2.lastRequest.toUpperCase();
			//return fruitName1.compareTo(fruitName2);
            return fruitName2.compareTo(fruitName1);
		}
	};
	
	public static Comparator<BasePrincipalSessions> NamaLengkapAsc =
			new Comparator<BasePrincipalSessions>() {
		public int compare(BasePrincipalSessions comp1, BasePrincipalSessions comp2) {
			String fruitName1 = comp1.principal.getLongName().toUpperCase();
			String fruitName2 = comp2.principal.getLongName().toUpperCase();
			return fruitName1.compareTo(fruitName2);
            //return fruitName2.compareTo(fruitName1);
		}
	};
	
	public static Comparator<BasePrincipalSessions> NamaLengkapDesc =
			new Comparator<BasePrincipalSessions>() {
		public int compare(BasePrincipalSessions comp1, BasePrincipalSessions comp2) {
			String fruitName1 = comp1.principal.getLongName().toUpperCase();
			String fruitName2 = comp2.principal.getLongName().toUpperCase();
			//return fruitName1.compareTo(fruitName2);
            return fruitName2.compareTo(fruitName1);
		}
	};
	
	public static Comparator<BasePrincipalSessions> RolesAsc =
			new Comparator<BasePrincipalSessions>() {
		public int compare(BasePrincipalSessions comp1, BasePrincipalSessions comp2) {
			String fruitName1 = comp1.principal.getRoleDesc().toUpperCase();
			String fruitName2 = comp2.principal.getRoleDesc().toUpperCase();
			return fruitName1.compareTo(fruitName2);
            //return fruitName2.compareTo(fruitName1);
		}
	};
	
	public static Comparator<BasePrincipalSessions> RolesDesc =
			new Comparator<BasePrincipalSessions>() {
		public int compare(BasePrincipalSessions comp1, BasePrincipalSessions comp2) {
			String fruitName1 = comp1.principal.getRoleDesc().toUpperCase();
			String fruitName2 = comp2.principal.getRoleDesc().toUpperCase();
			//return fruitName1.compareTo(fruitName2);
            return fruitName2.compareTo(fruitName1);
		}
	};
}
