package com.bank.mega.bean.principal;

import java.util.Comparator;

public class ComparablePrincipalSessions implements Comparable<ComparablePrincipalSessions> {
	private BasePrincipalSessions basePrincipalSessions;

	public BasePrincipalSessions getBasePrincipalSessions() {
		return basePrincipalSessions;
	}

	public void setBasePrincipalSessions(BasePrincipalSessions basePrincipalSessions) {
		this.basePrincipalSessions = basePrincipalSessions;
	}


	@Override
	public int compareTo(ComparablePrincipalSessions o) {
		// TODO Auto-generated method stub
		return 0;
	}

	public static Comparator<ComparablePrincipalSessions> USerIdComparatorAsc =
			new Comparator<ComparablePrincipalSessions>() {
		public int compare(ComparablePrincipalSessions comp1, ComparablePrincipalSessions comp2) {
			String fruitName1 = comp1.basePrincipalSessions.getPrincipal().getUserName().toUpperCase();
			String fruitName2 = comp2.basePrincipalSessions.getPrincipal().getUserName().toUpperCase();
			//ascending order
			return fruitName1.compareTo(fruitName2);
            //descending order
            //return fruitName2.compareTo(fruitName1);
		}

	};
}
