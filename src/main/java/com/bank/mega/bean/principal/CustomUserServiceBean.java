package com.bank.mega.bean.principal;

public class CustomUserServiceBean {
	    private String userName;
	    private boolean isActive = false;
	    private String firstLogin ;
	    private String longName;
	    private String roleCode;
	    private String roleDesc;
	    
	    
		public String getRoleCode() {
			return roleCode;
		}
		public void setRoleCode(String roleCode) {
			this.roleCode = roleCode;
		}
		public String getRoleDesc() {
			return roleDesc;
		}
		public void setRoleDesc(String roleDesc) {
			this.roleDesc = roleDesc;
		}
		public String getLongName() {
			return longName;
		}
		public void setLongName(String longName) {
			this.longName = longName;
		}
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public boolean isActive() {
			return isActive;
		}
		public void setActive(boolean isActive) {
			this.isActive = isActive;
		}
		public String getFirstLogin() {
			return firstLogin;
		}
		public void setFirstLogin(String firstLogin) {
			this.firstLogin = firstLogin;
		}
	    
	    
}
