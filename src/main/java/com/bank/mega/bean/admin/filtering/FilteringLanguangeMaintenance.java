package com.bank.mega.bean.admin.filtering;

public class FilteringLanguangeMaintenance {
	private String type;
	private String code;
	private String languange;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getLanguange() {
		return languange;
	}

	public void setLanguange(String languange) {
		this.languange = languange;
	}

}
