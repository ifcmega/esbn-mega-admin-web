package com.bank.mega.bean.admin;

import java.util.Date;


public class TblEsbnLoginLog {
    private Long logId;
	
	private String logUserId;
	
	private String logUserName;

	private String logUserTypeCode;
	
	private String logUserTypeCodeName;
	
	private Date logTglWaktuLogin;
	
	private String logTglFormatted;

	public Long getLogId() {
		return logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public String getLogUserId() {
		return logUserId;
	}

	public void setLogUserId(String logUserId) {
		this.logUserId = logUserId;
	}

	public String getLogUserName() {
		return logUserName;
	}

	public void setLogUserName(String logUserName) {
		this.logUserName = logUserName;
	}

	public String getLogUserTypeCode() {
		return logUserTypeCode;
	}

	public void setLogUserTypeCode(String logUserTypeCode) {
		this.logUserTypeCode = logUserTypeCode;
	}

	public String getLogUserTypeCodeName() {
		return logUserTypeCodeName;
	}

	public void setLogUserTypeCodeName(String logUserTypeCodeName) {
		this.logUserTypeCodeName = logUserTypeCodeName;
	}

	public Date getLogTglWaktuLogin() {
		return logTglWaktuLogin;
	}

	public void setLogTglWaktuLogin(Date logTglWaktuLogin) {
		this.logTglWaktuLogin = logTglWaktuLogin;
	}

	public String getLogTglFormatted() {
		return logTglFormatted;
	}

	public void setLogTglFormatted(String logTglFormatted) {
		this.logTglFormatted = logTglFormatted;
	}
	
	
}
