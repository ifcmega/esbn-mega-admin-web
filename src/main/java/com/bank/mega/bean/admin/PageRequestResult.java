package com.bank.mega.bean.admin;

import java.util.List;

public class PageRequestResult<T> {
     private List<T> content;
     private PageRequestBean pagination;
     private String query;
     private int totalData;
	 private int totalPage;
     
	public List<T> getContent() {
		return content;
	}
	public void setContent(List<T> content) {
		this.content = content;
	}
	public PageRequestBean getPagination() {
		return pagination;
	}
	public void setPagination(PageRequestBean pagination) {
		this.pagination = pagination;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public int getTotalData() {
		return totalData;
	}
	public void setTotalData(int totalData) {
		this.totalData = totalData;
	}
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	
	
     
     
}
