package com.bank.mega.bean.admin;

import java.util.List;

public class UploadMenuPagingBean {
     private String file;
     private String ext;
     private Double size;
     private Long totalData;
     private Long totalAccepted;
     private Long totalRejected;
     private String uploadBy;
     private List<TblAdminEsbnBeanUpload> uploadContent;
     
    
     
	public List<TblAdminEsbnBeanUpload> getUploadContent() {
		return uploadContent;
	}
	public void setUploadContent(List<TblAdminEsbnBeanUpload> uploadContent) {
		this.uploadContent = uploadContent;
	}
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public String getExt() {
		return ext;
	}
	public void setExt(String ext) {
		this.ext = ext;
	}
	public Double getSize() {
		return size;
	}
	public void setSize(Double size) {
		this.size = size;
	}
	public Long getTotalData() {
		return totalData;
	}
	public void setTotalData(Long totalData) {
		this.totalData = totalData;
	}
	public Long getTotalAccepted() {
		return totalAccepted;
	}
	public void setTotalAccepted(Long totalAccepted) {
		this.totalAccepted = totalAccepted;
	}
	public Long getTotalRejected() {
		return totalRejected;
	}
	public void setTotalRejected(Long totalRejected) {
		this.totalRejected = totalRejected;
	}
	public String getUploadBy() {
		return uploadBy;
	}
	public void setUploadBy(String uploadBy) {
		this.uploadBy = uploadBy;
	}
     
     
     
}
