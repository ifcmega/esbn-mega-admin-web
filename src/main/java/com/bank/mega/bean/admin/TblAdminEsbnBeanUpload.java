package com.bank.mega.bean.admin;

public class TblAdminEsbnBeanUpload {

	private String userSid;
	
	private String userName;
	
	private String email;
	
	private Boolean isLogin;
	
	private String roles;
	
	private Boolean enabled;
	
	private Boolean locked;
	
	private String rolesDesc;
	
	private boolean isAccepted;
	
	private String reasonUnaccepted;
	
	
	public boolean isAccepted() {
		return isAccepted;
	}

	public void setAccepted(boolean isAccepted) {
		this.isAccepted = isAccepted;
	}

	
	

	public String getReasonUnaccepted() {
		return reasonUnaccepted;
	}

	public void setReasonUnaccepted(String reasonUnaccepted) {
		this.reasonUnaccepted = reasonUnaccepted;
	}

	public String getRolesDesc() {
		return rolesDesc;
	}

	public void setRolesDesc(String rolesDesc) {
		this.rolesDesc = rolesDesc;
	}

	public String getUserSid() {
		return userSid;
	}

	public void setUserSid(String userSid) {
		this.userSid = userSid;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getIsLogin() {
		return isLogin;
	}

	public void setIsLogin(Boolean isLogin) {
		this.isLogin = isLogin;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean getLocked() {
		return locked;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}
	
	
}
