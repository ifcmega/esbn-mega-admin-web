package com.bank.mega.bean.admin;

import java.util.Date;

public class UserManagementNasabahBean {
	private String userSid;
	private String userRecognize;
	private String userName;
	private String password;
	private Boolean locked;
	private Integer loginFailed;
	private String otpVerification;
	private Boolean isActive;
	private Boolean isFirstEsbnRegist;
	private String userEmail;
	private String userPhone;
	private Date createdDate;
	private String createdBy;
	private Date lastUpdatedDate;
	private String lastUpdatedBy;
	private String cifNumber;
	private String createdDateString;
	private Boolean isLogin;

	
	public Boolean getIsLogin() {
		return isLogin;
	}

	public void setIsLogin(Boolean isLogin) {
		this.isLogin = isLogin;
	}

	public String getCreatedDateString() {
		return createdDateString;
	}

	public void setCreatedDateString(String createdDateString) {
		this.createdDateString = createdDateString;
	}

	public String getLastUpdatedDateString() {
		return lastUpdatedDateString;
	}

	public void setLastUpdatedDateString(String lastUpdatedDateString) {
		this.lastUpdatedDateString = lastUpdatedDateString;
	}

	private String lastUpdatedDateString;

	public String getUserSid() {
		return userSid;
	}

	public void setUserSid(String userSid) {
		this.userSid = userSid;
	}

	public String getUserRecognize() {
		return userRecognize;
	}

	public void setUserRecognize(String userRecognize) {
		this.userRecognize = userRecognize;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getLocked() {
		return locked;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}

	public Integer getLoginFailed() {
		return loginFailed;
	}

	public void setLoginFailed(Integer loginFailed) {
		this.loginFailed = loginFailed;
	}

	public String getOtpVerification() {
		return otpVerification;
	}

	public void setOtpVerification(String otpVerification) {
		this.otpVerification = otpVerification;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsFirstEsbnRegist() {
		return isFirstEsbnRegist;
	}

	public void setIsFirstEsbnRegist(Boolean isFirstEsbnRegist) {
		this.isFirstEsbnRegist = isFirstEsbnRegist;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getCifNumber() {
		return cifNumber;
	}

	public void setCifNumber(String cifNumber) {
		this.cifNumber = cifNumber;
	}

}
