package com.bank.mega.bean.admin;

import java.util.List;

public class ResultUploadDataUserListBean {
    private Long totalData;
    private Long totalDataAccept;
    private Long totalDataUnaccept;
    private List<TblAdminEsbnBeanUpload> content;
	public Long getTotalData() {
		return totalData;
	}
	public void setTotalData(Long totalData) {
		this.totalData = totalData;
	}
	public Long getTotalDataAccept() {
		return totalDataAccept;
	}
	public void setTotalDataAccept(Long totalDataAccept) {
		this.totalDataAccept = totalDataAccept;
	}
	public Long getTotalDataUnaccept() {
		return totalDataUnaccept;
	}
	public void setTotalDataUnaccept(Long totalDataUnaccept) {
		this.totalDataUnaccept = totalDataUnaccept;
	}
	public List<TblAdminEsbnBeanUpload> getContent() {
		return content;
	}
	public void setContent(List<TblAdminEsbnBeanUpload> content) {
		this.content = content;
	}
    
    
}
