package com.bank.mega.bean.admin.transaction;

import com.bank.mega.bean.admin.TblMasterWordingEsbnBean;

public class WsResponseTransactionResultLang {
  
	private TblMasterWordingEsbnBean content;
	private Boolean isSuccess;
	private String reason;
	
	
	
	public TblMasterWordingEsbnBean getContent() {
		return content;
	}
	public void setContent(TblMasterWordingEsbnBean content) {
		this.content = content;
	}
	public Boolean getIsSuccess() {
		return isSuccess;
	}
	public void setIsSuccess(Boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	
	
}
