package com.bank.mega.bean.admin;

import java.util.List;

public class ReportUserLogin {
	private List<Object[]> totalLoginEachDate;
	private Long totalSistemLogin;
	private Long totalLoginNasabah;
	 private String query;
     private int totalData;
	 private int totalPage;
	 private Boolean isError;
	 private String reason;
	 private int page;
     private int size;
	public List<Object[]> getTotalLoginEachDate() {
		return totalLoginEachDate;
	}
	public void setTotalLoginEachDate(List<Object[]> totalLoginEachDate) {
		this.totalLoginEachDate = totalLoginEachDate;
	}
	public Long getTotalSistemLogin() {
		return totalSistemLogin;
	}
	public void setTotalSistemLogin(Long totalSistemLogin) {
		this.totalSistemLogin = totalSistemLogin;
	}
	public Long getTotalLoginNasabah() {
		return totalLoginNasabah;
	}
	public void setTotalLoginNasabah(Long totalLoginNasabah) {
		this.totalLoginNasabah = totalLoginNasabah;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public int getTotalData() {
		return totalData;
	}
	public void setTotalData(int totalData) {
		this.totalData = totalData;
	}
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	public Boolean getIsError() {
		return isError;
	}
	public void setIsError(Boolean isError) {
		this.isError = isError;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
     
     
}
