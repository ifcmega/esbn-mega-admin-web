package com.bank.mega.bean.admin;

import java.util.List;

public class PagingRequestLanguange {
	private List<TblMasterWordingEsbnBean> content;
	private PageRequestBean pagination;
	private String query;
	private int totalData;
	private int totalPage;
	private Boolean isError;
	private String reason;
	public List<TblMasterWordingEsbnBean> getContent() {
		return content;
	}
	public void setContent(List<TblMasterWordingEsbnBean> content) {
		this.content = content;
	}
	public PageRequestBean getPagination() {
		return pagination;
	}
	public void setPagination(PageRequestBean pagination) {
		this.pagination = pagination;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public int getTotalData() {
		return totalData;
	}
	public void setTotalData(int totalData) {
		this.totalData = totalData;
	}
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	public Boolean getIsError() {
		return isError;
	}
	public void setIsError(Boolean isError) {
		this.isError = isError;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	
}
