package com.bank.mega.bean.admin;

import java.util.Date;


public class TblEsbnLoginLogBean {
	private Long logId;
	private String logUserId;
	private String logUserName;
	private String logUserTypeCode;
	private String logUserTypeCodeName;
	private Date logTglWaktuLogin;

	public Long getLogId() {
		return logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	
	public String getLogUserId() {
		return logUserId;
	}

	public void setLogUserId(String logUserId) {
		this.logUserId = logUserId;
	}

	public String getLogUserTypeCode() {
		return logUserTypeCode;
	}

	public void setLogUserTypeCode(String logUserTypeCode) {
		this.logUserTypeCode = logUserTypeCode;
	}

	public String getLogUserTypeCodeName() {
		return logUserTypeCodeName;
	}

	public void setLogUserTypeCodeName(String logUserTypeCodeName) {
		this.logUserTypeCodeName = logUserTypeCodeName;
	}

	public String getLogUserName() {
		return logUserName;
	}

	public void setLogUserName(String logUserName) {
		this.logUserName = logUserName;
	}

	public Date getLogTglWaktuLogin() {
		return logTglWaktuLogin;
	}

	public void setLogTglWaktuLogin(Date logTglWaktuLogin) {
		this.logTglWaktuLogin = logTglWaktuLogin;
	}
	
	
}
