package com.bank.mega.bean.admin.master;

import java.util.Date;


public class TblMasterPictureBean {
	private long reffId;

	private String linkUrl;
	
	private Date createdDate;
	
	private String createdBy;
	
	private Date updatedDate;
	
	private String updatedBy;
	
	private String pictureType;
	
	private String directedUrl;
	
	

	public String getDirectedUrl() {
		return directedUrl;
	}

	public void setDirectedUrl(String directedUrl) {
		this.directedUrl = directedUrl;
	}

	public long getReffId() {
		return reffId;
	}

	public void setReffId(long reffId) {
		this.reffId = reffId;
	}

	public String getLinkUrl() {
		return linkUrl;
	}

	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getPictureType() {
		return pictureType;
	}

	public void setPictureType(String pictureType) {
		this.pictureType = pictureType;
	}
	
	
}
