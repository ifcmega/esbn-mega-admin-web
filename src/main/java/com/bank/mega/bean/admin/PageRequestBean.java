package com.bank.mega.bean.admin;

import java.util.Map;

public class PageRequestBean {
     private String search;
     private String searchKolom;
     private int page;
     private int size;
     private String sortBy;
     private String sortCondition;
     private Map<String, Object> filtering;
    
    
     
	public Map<String, Object> getFiltering() {
		return filtering;
	}
	public void setFiltering(Map<String, Object> filtering) {
		this.filtering = filtering;
	}
	public String getSearchKolom() {
		return searchKolom;
	}
	public void setSearchKolom(String searchKolom) {
		this.searchKolom = searchKolom;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public String getSortBy() {
		return sortBy;
	}
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	public String getSortCondition() {
		return sortCondition;
	}
	public void setSortCondition(String sortCondition) {
		this.sortCondition = sortCondition;
	}
	
     
}
