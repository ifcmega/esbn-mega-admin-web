package com.bank.mega.bean.admin;

import java.util.Date;

public class UserManagementUserListBean {
	  private String userId;
	    private String roleCode;
	    private String namaLengkap;
	    private String email;
	    private Boolean isLogin;
	    private String roles;
	    private Boolean enabled;
	    private Boolean locked;
	    private Date createdDate;
	    private String createdDateString;
	    private Date lastUpdatedDate;
	    private String lastUpdatedDateString;
	    private String createdBy;
	    private String lastUpdatedBy;
	    private String userName;
	    
	    
	    
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public String getUserId() {
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
		public String getRoleCode() {
			return roleCode;
		}
		public void setRoleCode(String roleCode) {
			this.roleCode = roleCode;
		}
		public String getNamaLengkap() {
			return namaLengkap;
		}
		public void setNamaLengkap(String namaLengkap) {
			this.namaLengkap = namaLengkap;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		
		
		public Boolean getIsLogin() {
			return isLogin;
		}
		public void setIsLogin(Boolean isLogin) {
			this.isLogin = isLogin;
		}
		public String getRoles() {
			return roles;
		}
		public void setRoles(String roles) {
			this.roles = roles;
		}
		public Boolean getEnabled() {
			return enabled;
		}
		public void setEnabled(Boolean enabled) {
			this.enabled = enabled;
		}
		public Boolean getLocked() {
			return locked;
		}
		public void setLocked(Boolean locked) {
			this.locked = locked;
		}
		public Date getCreatedDate() {
			return createdDate;
		}
		public void setCreatedDate(Date createdDate) {
			this.createdDate = createdDate;
		}
		public String getCreatedDateString() {
			return createdDateString;
		}
		public void setCreatedDateString(String createdDateString) {
			this.createdDateString = createdDateString;
		}
		public Date getLastUpdatedDate() {
			return lastUpdatedDate;
		}
		public void setLastUpdatedDate(Date lastUpdatedDate) {
			this.lastUpdatedDate = lastUpdatedDate;
		}
		public String getLastUpdatedDateString() {
			return lastUpdatedDateString;
		}
		public void setLastUpdatedDateString(String lastUpdatedDateString) {
			this.lastUpdatedDateString = lastUpdatedDateString;
		}
		public String getCreatedBy() {
			return createdBy;
		}
		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}
		public String getLastUpdatedBy() {
			return lastUpdatedBy;
		}
		public void setLastUpdatedBy(String lastUpdatedBy) {
			this.lastUpdatedBy = lastUpdatedBy;
		}
    
    
    
    
}
