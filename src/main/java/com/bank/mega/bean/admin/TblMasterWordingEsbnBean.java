package com.bank.mega.bean.admin;


public class TblMasterWordingEsbnBean {

    private String refferenceId;   //idKey
	
    private String wordingCode;    //grup kode yang akan dipanggil
	
    private String wordingType;    //kategori
	
    private String wordingDetail;  //text
	
    private String wordingLanguangeId;

	public String getRefferenceId() {
		return refferenceId;
	}

	public void setRefferenceId(String refferenceId) {
		this.refferenceId = refferenceId;
	}

	public String getWordingCode() {
		return wordingCode;
	}

	public void setWordingCode(String wordingCode) {
		this.wordingCode = wordingCode;
	}

	public String getWordingType() {
		return wordingType;
	}

	public void setWordingType(String wordingType) {
		this.wordingType = wordingType;
	}

	public String getWordingDetail() {
		return wordingDetail;
	}

	public void setWordingDetail(String wordingDetail) {
		this.wordingDetail = wordingDetail;
	}

	public String getWordingLanguangeId() {
		return wordingLanguangeId;
	}

	public void setWordingLanguangeId(String wordingLanguangeId) {
		this.wordingLanguangeId = wordingLanguangeId;
	}  
    
    
}
