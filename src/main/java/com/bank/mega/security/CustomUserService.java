package com.bank.mega.security;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.bank.mega.bean.admin.TblAdminEsbnBean;
import com.google.gson.Gson;

public class CustomUserService implements UserDetails {

	private static final long serialVersionUID = -5446817407722834700L;
    private String userName;
    private String password;
    private boolean isActive = false;
    private String firstLogin = new Gson().toJson(new Date());
    Collection<? extends GrantedAuthority> authorities;
    private String longName;
    private String roleCode;
    private String roleDesc;
    
    
	public CustomUserService() {
		super();
	}

	public CustomUserService(TblAdminEsbnBean tblUser,String password) {
		//PasswordEncoder encoder = new BCryptPasswordEncoder();
		  this.userName = tblUser.getUserSid();
		  this.password = password;
		  this.roleCode = tblUser.getRoles();
		  this.roleDesc = tblUser.getRolesDesc();
		  System.err.println("cek : " + this.userName + "   " + this.password);
	      this.isActive = tblUser.getEnabled();
		  this.longName = tblUser.getUserName();
		  List<GrantedAuthority> authorities = new ArrayList<>();
			  authorities.add(new SimpleGrantedAuthority("ADMIN"));
		  this.authorities = authorities;
	}
	
	
	
	public String getRoleCode() {
		return roleCode;
	}

	public String getRoleDesc() {
		return roleDesc;
	}

	public String getLongName() {
		return longName;
	}

	public String getFirstLogin() {
		return firstLogin;
	}

	public void setFirstLogin(String firstLogin) {
		this.firstLogin = firstLogin;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return authorities;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return password;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return userName;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return isActive;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	
	

}
