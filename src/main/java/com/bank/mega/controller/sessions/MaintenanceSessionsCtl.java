package com.bank.mega.controller.sessions;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bank.mega.service.BaseService;

@Controller
public class MaintenanceSessionsCtl extends BaseService{

	
	@RequestMapping("/sessions")
	public String sessionsMonitoring(Model model, Authentication authentication, HttpServletRequest request, HttpServletResponse response) {
		informationHeader(model, authentication);
		
		return checkSessionValidity("sessions/maintenance-sessions",authentication, 
				request, response);
	}
	
}
