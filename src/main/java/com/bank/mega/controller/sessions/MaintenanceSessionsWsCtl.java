package com.bank.mega.controller.sessions;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.codehaus.groovy.runtime.ArrayUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bank.mega.bean.principal.BasePrincipalSessions;
import com.bank.mega.security.CustomUserService;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.service.BaseService;
import com.google.gson.Gson;
import org.springframework.http.MediaType;
@RestController
@RequestMapping("/maintenaceSessions")
public class MaintenanceSessionsWsCtl extends BaseService{

    @GetMapping("/killedSession")
    public void killedSession(Authentication authentication, HttpServletRequest request,HttpServletResponse response) {
    	checkSessionValidity(authentication, request, response);
    	for (Object principal : sessionRegistry.getAllPrincipals()) {
			if (principal instanceof CustomUserService) {
				CustomUserService service = (CustomUserService) principal;
				if (service.getUsername().equalsIgnoreCase("uatstaff2")) {
					System.err.println("session uatstaf 2 : "+new Gson().toJson(sessionRegistry.getAllSessions(principal, true)));
					 List<BasePrincipalSessions> list = new ArrayList<>();
					try {
						list = mapperJsonToListDto(new Gson().toJson(sessionRegistry.getAllSessions
								 (principal, true))
								 , BasePrincipalSessions.class);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					 sessionRegistry.removeSessionInformation(list.get(0).getSessionId());
				}
			}
		}
    }
	
    private List<BasePrincipalSessions> searchSession(
    		List<BasePrincipalSessions> basePrincipalSessionsDeb,String kolom,String search){
 
    	List<BasePrincipalSessions> basePrincipalSessions = new ArrayList<>();		
    	if(kolom == null || search == null) {
          return basePrincipalSessionsDeb;
    	}
    	
    	search = search.toUpperCase();
    	System.err.println(new Gson().toJson(basePrincipalSessionsDeb));
    	for (BasePrincipalSessions base : basePrincipalSessionsDeb) {
			if(kolom.equals("userid")&& base.getPrincipal()!=null && 
					  base.getPrincipal().getUserName().toUpperCase().contains(search)) {
				basePrincipalSessions.add(base);
			}
			else if(kolom.equals("token")&& base.getSessionId().toUpperCase().contains(search)) {
				basePrincipalSessions.add(base);
			}
//			else if(kolom.equals("host")&& base.getSessionId().equalsIgnoreCase(search)) {
//				basePrincipalSessionsDeb.add(base);
//			}
			else if(kolom.equals("waktulogin")&& base.getPrincipal()!=null && 
					base.getFirstLoginInFormat().toUpperCase().contains(search)) {
				basePrincipalSessions.add(base);
			}
			else if(kolom.equals("aktivitasterakhir")&& 
					base.getLastRequestOnFormat().toUpperCase().contains(search)) {
				basePrincipalSessions.add(base);
			}
			else if(kolom.equals("namalengkap")&& base.getPrincipal()!=null && 
					base.getPrincipal().getLongName().toUpperCase().contains(search)) {
				basePrincipalSessions.add(base);
			}
			else if(kolom.equals("roles")&& base.getPrincipal()!=null && 
					base.getPrincipal().getRoleDesc().toUpperCase().contains(search)) {
				basePrincipalSessions.add(base);
			}
		}
    	return basePrincipalSessions;
    }
    
    @PostMapping("/retrieve-sort")
    public Map<String,Object> getAllSessionSort(
    		@RequestBody Map<String, String> mapp, Authentication authentication){
    	Map<String,Object> map = new HashMap<>();
    	List<BasePrincipalSessions> basePrincipalSessions = getAllSessionsGlobal(authentication);
    	String kolom = mapp.get("kolom");
    	String search = mapp.get("search");
    	String page = mapp.get("page");
    	String sort = mapp.get("sort");
    	String kolomSort = mapp.get("kolomSort");
    	int pagint = Integer.parseInt(page);
    		basePrincipalSessions = sortExceedData(basePrincipalSessions, kolomSort, sort);
    		List<BasePrincipalSessions> basePrincipalSessionsPaging = new ArrayList<>();
    		basePrincipalSessionsPaging = getAllSessionsGlobal(authentication);
    	if(limitedExceedData(searchSession(basePrincipalSessions,kolom, search), pagint, COUNTER_PAGING)!=null) {
    		basePrincipalSessions = limitedExceedData(searchSession(basePrincipalSessions,kolom, search), pagint, COUNTER_PAGING);
    	}
     
    	
    	map.put("data", basePrincipalSessions);
		map.put("pageNumber", 1);
		map.put("total", searchSession( getAllSessionsGlobal(authentication),kolom, search).size());
		map.put("counter", COUNTER_PAGING);
		map.put("totalPage", Math.ceil((double)searchSession(basePrincipalSessionsPaging,kolom, search).size()/COUNTER_PAGING));
    	return map;
    }
    
    @PostMapping("/retrieve-search")
    public Map<String,Object> getAllSessionSearch(@RequestBody 
    		Map<String, String> mapp,Authentication authentication){
    	Map<String,Object> map = new HashMap<>();
    	List<BasePrincipalSessions> basePrincipalSessions = new ArrayList<>();
    	String kolom = mapp.get("kolom");
    	String search = mapp.get("search");
    	String page = mapp.get("page");
    	int pagint = Integer.parseInt(page);
    	String sort = mapp.get("sort");
    	String kolomSort = mapp.get("kolomSort");
    	basePrincipalSessions = getAllSessionsGlobal(authentication);
    	basePrincipalSessions = sortExceedData(basePrincipalSessions, kolomSort, sort);
    	List<BasePrincipalSessions> basePrincipalSessionsPaging = new ArrayList<>();
		basePrincipalSessionsPaging = getAllSessionsGlobal(authentication);
    	if(limitedExceedData(searchSession(basePrincipalSessions,kolom, search), pagint, COUNTER_PAGING)!=null) {
    		basePrincipalSessions = limitedExceedData(searchSession(basePrincipalSessions,kolom, search), pagint, COUNTER_PAGING);
    	}
    	map.put("data",  basePrincipalSessions);
		map.put("pageNumber", 1);
		map.put("total", searchSession( getAllSessionsGlobal(authentication),kolom, search).size());
		map.put("counter", COUNTER_PAGING);
		map.put("totalPage", Math.ceil((double)searchSession(basePrincipalSessionsPaging,kolom, search).size()/COUNTER_PAGING));
    	return map;
    }
    
    @PostMapping("/retrieve-paging")
    public Map<String, Object> getAllSessionsPage(@RequestBody 
    		Map<String, String> mapp,Authentication authentication){
    	List<BasePrincipalSessions> basePrincipalSessions = new ArrayList<>();
		basePrincipalSessions = getAllSessionsGlobal(authentication);
		List<BasePrincipalSessions> basePrincipalSessionsPaging = new ArrayList<>();
		basePrincipalSessionsPaging = getAllSessionsGlobal(authentication);
		String kolom = mapp.get("kolom");
    	String search = mapp.get("search");
    	String sort = mapp.get("sort");
    	String kolomSort = mapp.get("kolomSort");
		int page = Integer.parseInt(mapp.get("page"));
		basePrincipalSessions = sortExceedData(basePrincipalSessions, kolomSort, sort);
		if(limitedExceedData(searchSession(basePrincipalSessions,kolom, search), page, COUNTER_PAGING)!=null) {
    		basePrincipalSessions = limitedExceedData(searchSession(basePrincipalSessions,kolom, search), page, COUNTER_PAGING);
    	}
		Map<String, Object> map = new HashMap<>();
		map.put("data",basePrincipalSessions);
		map.put("pageNumber", page);
		map.put("total", searchSession( getAllSessionsGlobal(authentication),kolom, search).size());
		map.put("counter", COUNTER_PAGING);
		map.put("totalPage", Math.ceil((double)searchSession(basePrincipalSessionsPaging,kolom, search).size()/COUNTER_PAGING));

		return map;
    }
    
	@GetMapping("/retrieve")
	public Map<String, Object> getAllSessions(Authentication authentication, 
			Model model,HttpServletRequest request){
		List<BasePrincipalSessions> basePrincipalSessions = new ArrayList<>();
		List<BasePrincipalSessions> basePrincipalSessionsPaging = new ArrayList<>();
		basePrincipalSessionsPaging = getAllSessionsGlobal(authentication);
		basePrincipalSessions = getAllSessionsGlobal(authentication);
		
		if(limitedExceedData(searchSession(basePrincipalSessions,null, null), 1, COUNTER_PAGING)!=null) {
    		basePrincipalSessions = limitedExceedData(searchSession(basePrincipalSessions,null, null), 1, COUNTER_PAGING);
    	}
		Map<String, Object> map = new HashMap<>();
		map.put("data",basePrincipalSessions);
		map.put("pageNumber", 1);
		map.put("total", searchSession( getAllSessionsGlobal(authentication),null, null).size());
		map.put("counter", COUNTER_PAGING);
		map.put("totalPage", Math.ceil((double)searchSession(basePrincipalSessionsPaging,null, null).size()/COUNTER_PAGING));

		return map;
	}
	
	public static File folder = new File(System.getProperty("user.dir") + "/temp/");
	public static Path path = Paths.get(String.valueOf(folder));

	

	@RequestMapping(value = "/downloadableFile", method = RequestMethod.GET)
	public ResponseEntity<byte[]> download(Authentication authentication,Model model, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(baos));
		        

		            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT
		                    .withHeader("No.", "User Id", "Token", "Host","Waktu Login","Aktivitas Terakhir",
		                    		"Nama Lengkap","Role"));
		       
		            int i = 1;
		            for (BasePrincipalSessions sess : getAllSessionsGlobal(authentication)) {
						csvPrinter.printRecord(i,sess.getPrincipal().getUserName(),sess.getSessionId(),
								"",sess.getPrincipal().getFirstLogin(),sess.getLastRequest(),
								sess.getPrincipal().getLongName(),sess.getPrincipal().getRoleDesc()); 	
		            	i++;
					}
		            
//		            csvPrinter.printRecord("1", "Sundar Pichai ♥", "CEO", "Google");
//		            csvPrinter.printRecord("2", "Satya Nadella", "CEO", "Microsoft");
//		            csvPrinter.printRecord("3", "Tim cook", "CEO", "Apple");
//
//		            csvPrinter.printRecord(Arrays.asList("4", "Mark Zuckerberg", "CEO", "Facebook"));

		            csvPrinter.flush();            
		        
		            
		            
		            
		ResponseEntity<byte[]> rsp = new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);
	
			HttpHeaders responseHeaders = new HttpHeaders();
			responseHeaders.setContentType(MediaType.parseMediaType("text/plain; charset=utf-8"));
			responseHeaders.set("Content-Disposition", "attachment");
			responseHeaders.setContentDispositionFormData("List User Login Esbn Web", "List User Login Esbn Web");
			responseHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			rsp = new ResponseEntity<byte[]>(baos.toByteArray(), responseHeaders, HttpStatus.OK);
		
		return rsp;
	}
	
	private  List<BasePrincipalSessions> sortExceedData(List<BasePrincipalSessions> basePrincipalSessions,
			String kolomSort, String sort){
		if(basePrincipalSessions!=null&&kolomSort!=null&&sort!=null) {
		    BasePrincipalSessions[] bas =  basePrincipalSessions.toArray(new BasePrincipalSessions[basePrincipalSessions.size()]);
	    	if(kolomSort.equals("userid")&&sort.equals("asc")) {
		    Arrays.sort(bas,BasePrincipalSessions.UserIdComparatorAsc);
	    	}
	    	else if(kolomSort.equals("userid")&&sort.equals("desc")) {
	    		 Arrays.sort(bas,BasePrincipalSessions.UserIdComparatorDesc);
	    	}
	    	else if(kolomSort.equals("token")&&sort.equals("asc")) {
	    		 Arrays.sort(bas,BasePrincipalSessions.TokenAsc);
	    	}
	    	else if(kolomSort.equals("token")&&sort.equals("desc")) {
	    		 Arrays.sort(bas,BasePrincipalSessions.TokenDesc);
	    	}
	    	else if(kolomSort.equals("waktulogin")&&sort.equals("asc")) {
	    		 Arrays.sort(bas,BasePrincipalSessions.WaktuLoginAsc);
	    	}
	    	else if(kolomSort.equals("waktulogin")&&sort.equals("desc")) {
	    		 Arrays.sort(bas,BasePrincipalSessions.WaktuLoginDesc);
	    	}
	    	else if(kolomSort.equals("aktivitasterakhir")&&sort.equals("asc")) {
	    		 Arrays.sort(bas,BasePrincipalSessions.AktivitasTerakhirAsc);
	    	}
	    	else if(kolomSort.equals("aktivitasterakhir")&&sort.equals("desc")) {
	    		 Arrays.sort(bas,BasePrincipalSessions.AktivitasTerakhirDesc);
	    	}
	    	else if(kolomSort.equals("namalengkap")&&sort.equals("asc")) {
	    		 Arrays.sort(bas,BasePrincipalSessions.NamaLengkapAsc);
	    	}
	    	else if(kolomSort.equals("namalengkap")&&sort.equals("desc")) {
	    		 Arrays.sort(bas,BasePrincipalSessions.NamaLengkapDesc);
	    	}
	    	else if(kolomSort.equals("roles")&&sort.equals("asc")) {
	    		 Arrays.sort(bas,BasePrincipalSessions.RolesAsc);
	    	}
	    	else if(kolomSort.equals("roles")&&sort.equals("desc")) {
	    		 Arrays.sort(bas,BasePrincipalSessions.RolesDesc);
	    	}
	    	return Arrays.asList(bas);
		}
		else {
			return basePrincipalSessions;
		}
	}
	
	private List<BasePrincipalSessions> limitedExceedData(List<BasePrincipalSessions> basePrincipalSessions,
			int paging, int counterPaging)
	{
		 List<BasePrincipalSessions> sessions = new ArrayList<>();
		 
		 int i = 1;
		 for (BasePrincipalSessions sess : basePrincipalSessions) {
		     if((i==1&&i>=counterPaging*(paging-1)&&i<=basePrincipalSessions.size()
		    		 && i <= counterPaging*paging)||
		    		 (i!=1&&i>=(counterPaging*(paging-1)+1)&&i<=basePrincipalSessions.size()
		    		 && i <= counterPaging*paging)) {
		    	 sessions.add(sess);
		     } 
		     i++;
		 }
		 
		 return sessions;
	}
	
	
}
