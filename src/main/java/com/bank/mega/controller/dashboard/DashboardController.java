package com.bank.mega.controller.dashboard;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.service.BaseService;
import com.google.gson.Gson;

@Controller
public class DashboardController extends BaseService {

	@RequestMapping("/")
	public String index(Model model, Authentication auth) {
		informationHeader(model, auth);
		return "dashboard/index";
	}

}
