package com.bank.mega.controller.master;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bank.mega.service.BaseService;

@Controller
public class MasterPictureCtl extends BaseService {

	@RequestMapping("/master-picture")
	public String goToMasterPicture(Model model, Authentication authentication, HttpServletRequest request,
			HttpServletResponse response) {
		informationHeader(model, authentication);

		return checkSessionValidity("master/masterPicture", authentication, request, response);
	}
}
