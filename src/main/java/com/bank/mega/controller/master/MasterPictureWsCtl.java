package com.bank.mega.controller.master;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bank.mega.bean.admin.ResultUploadDataUserListBean;
import com.bank.mega.bean.admin.TblAdminEsbnBeanUpload;
import com.bank.mega.bean.admin.UploadMenuPagingBean;
import com.bank.mega.bean.admin.master.TblMasterPictureBean;
import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.controller.BaseController;
import com.bank.mega.security.HttpRestResponse;
import com.google.gson.Gson;

@RestController
@RequestMapping("/shared/master-picture")
public class MasterPictureWsCtl extends BaseController{

	private static final String FOLDER = "/home/cobs-admin/ESBN/development/picture";
	//private static final String FOLDER = "/home/iforce-86/Desktop/picture-tester-esbn";
	
	@GetMapping("/get-all-master-carousel")
	public List<TblMasterPictureBean> getAllCarousel(){
		HttpRestResponse response = wsBody(wmsParentalWrapper+"/shared/master-picture/get-carousel",null , 
				HttpMethod.GET, new HashMap<>());
		try {
			return mapperJsonToListDto(response.getBody(), TblMasterPictureBean.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
			
		}
	}
	
	@GetMapping("/deleteFile/{reffId}")
	public Map<String, Object> deleteFileDownload(
			@PathVariable("reffId") Long reffId,
			Authentication authentication, HttpSession session) {
		HttpRestResponse response = wsBody(wmsParentalWrapper+
				"/shared/master-picture/delete-picture/"+reffId+"/"+authentication.getName(), 
				null,
				HttpMethod.GET, new HashMap<>());
		
		return mapperJsonToHashMap(response.getBody());
	}
	
	@PostMapping("/uploadFile")
	public Map<String, Object> saveFileDownload(@RequestBody Map<String, Object> dataUrl,
			Authentication authentication, HttpSession session) {
		String dataUri = (String) dataUrl.get("dataUri");
		String title = (String) dataUrl.get("name");
		Integer intLo = (Integer) dataUrl.get("number");
		String dataLink = (String) dataUrl.get("dataLink");
		Long reffid = intLo.longValue();
		byte[] fileData = DatatypeConverter.parseBase64Binary(dataUri.substring(dataUri.indexOf(",") + 1));
		
		Map<String, String> map = new HashMap<>();
		map.put("dataLink", dataLink);
		HttpRestResponse response = wsBody(wmsParentalWrapper+"/shared/master-picture/save-picture/"+reffid+"/"+authentication.getName()+"?title="+title, 
				fileData,
				HttpMethod.POST, map);
		
		return mapperJsonToHashMap(response.getBody());
	}
	
	@PostMapping("/addFile")
	public Map<String, Object> addFileDownload(@RequestBody Map<String, Object> dataUrl,
			Authentication authentication, HttpSession session) {
		String dataUri = (String) dataUrl.get("dataUri");
		String title = (String) dataUrl.get("name");
		String dataLink = (String) dataUrl.get("dataLink");
		byte[] fileData = DatatypeConverter.parseBase64Binary(dataUri.substring(dataUri.indexOf(",") + 1));
		
		Map<String, String> map = new HashMap<>();
		map.put("dataLink", dataLink);
		HttpRestResponse response = wsBody(wmsParentalWrapper+"/shared/master-picture/add-picture-carousel/"+authentication.getName()+"?title="+title, 
				fileData,
				HttpMethod.POST, map);
		
		return mapperJsonToHashMap(response.getBody());
	}
	
	@GetMapping(value = "/picture")
	public void getFile(
			@RequestParam(name = "draft") String pathVariable,
			HttpServletResponse httpServletResponse) {
		try {
			System.err.println("path yang dipanggil : " + pathVariable );
		    
			String finalUrl = FOLDER + pathVariable;
			//"/home/iforce-86/Pictures/262689_1.jpg"
	        InputStream in = new FileInputStream(new File(finalUrl));
	        HttpHeaders headers = new HttpHeaders();
	        headers.setCacheControl(CacheControl.noCache().getHeaderValue());
	        httpServletResponse.setContentType(MediaType.IMAGE_JPEG_VALUE);
	        IOUtils.copy(in, httpServletResponse.getOutputStream());
	      //  return new ResponseEntity<>(new File("/home/iforce-86/Pictures/262689_1.jpg"), headers, HttpStatus.OK);
	    } catch (IOException e) {
	        //logger.error(e);
	        throw new RuntimeException(e);
	    }
	}
}
