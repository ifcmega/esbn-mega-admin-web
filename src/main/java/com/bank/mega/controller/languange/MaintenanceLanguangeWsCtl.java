package com.bank.mega.controller.languange;

import java.util.Map;

import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bank.mega.bean.admin.PageRequestBean;
import com.bank.mega.bean.admin.PagingRequestLanguange;
import com.bank.mega.bean.admin.TblMasterWordingEsbnBean;
import com.bank.mega.bean.admin.filtering.FilteringLanguangeMaintenance;
import com.bank.mega.bean.admin.transaction.WsResponseTransactionResultLang;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.service.BaseService;

@RestController
@RequestMapping("/maintenaceLanguanges")
public class MaintenanceLanguangeWsCtl extends BaseService{
    
	@PostMapping("/update-my-languange")
	public WsResponseTransactionResultLang putUpdateLanguange(
			Authentication authentication,@RequestBody TblMasterWordingEsbnBean tblMasterWordingEsbnBean) {
		Map<String, String> map = getMyRequestToken(authentication);
		HttpRestResponse  response = wsBody
				(wmsParentalWrapper+"/languangeManagementCtl/update-data-languange"
						,tblMasterWordingEsbnBean,HttpMethod.PUT, 
				map);
		WsResponseTransactionResultLang lang = new WsResponseTransactionResultLang();
		try {
			lang = mapperJsonToSingleDto(response.getBody(), WsResponseTransactionResultLang.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return lang;
	}
	
	@PostMapping("/retrieve-all-languange")
	public PagingRequestLanguange retrieveAllLanguange(@RequestParam("page")int page
			,@RequestParam("sort") String sort
			,@RequestBody Map<String, Object> filtering
			,Authentication authentication) {
		PagingRequestLanguange languange = new PagingRequestLanguange();
		String url = "/languangeManagementCtl/retrieve-all-wording?"
		+"page="+(page-1)+"&size="+COUNTER_PAGING+"&sort="+sort;
		Map<String, String> map = getMyRequestToken(authentication);
		PageRequestBean pageRequestBean = new PageRequestBean();
		pageRequestBean.setFiltering(filtering);
		HttpRestResponse  response = wsBody(wmsParentalWrapper+url,pageRequestBean, HttpMethod.POST, 
				map);
		
		try {
			languange = mapperJsonToSingleDto(response.getBody(), PagingRequestLanguange.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return languange;
	}
}
