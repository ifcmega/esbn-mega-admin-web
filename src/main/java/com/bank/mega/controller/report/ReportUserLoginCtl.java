package com.bank.mega.controller.report;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bank.mega.service.BaseService;

@Controller
public class ReportUserLoginCtl extends BaseService{
	
	@RequestMapping("/report-login")
	public String sessionsMonitoring(Model model, Authentication authentication, HttpServletRequest request, HttpServletResponse response) {
		informationHeader(model, authentication);
		
		return checkSessionValidity("report/ReportUserLogin",authentication, 
				request, response);
	}
	@RequestMapping("/report-login-nasabah")
	public String sessionsMonitoringNasabah(Model model, Authentication authentication, HttpServletRequest request, HttpServletResponse response) {
		informationHeader(model, authentication);
		
		return checkSessionValidity("report/ReportNasabahLogin",authentication, 
				request, response);
	}
}
