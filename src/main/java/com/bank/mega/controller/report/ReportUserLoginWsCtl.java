package com.bank.mega.controller.report;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bank.mega.bean.admin.ReportUserLogin;
import com.bank.mega.bean.admin.TblAdminEsbnBeanUpload;
import com.bank.mega.bean.admin.TblEsbnLoginLog;
import com.bank.mega.bean.admin.UserManagementUserListBean;
import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.bean.log.SmsLogBean;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.service.BaseService;
import com.google.gson.Gson;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

@RestController
@RequestMapping("/reportUserLoginWsCtl")
public class ReportUserLoginWsCtl extends BaseService {

	@PostMapping("/retrieve-all-report/{role}")
	public ReportUserLogin callAllReport(@PathVariable("role") String role, @RequestParam("page") int page,
			@RequestBody Map<String, Object> mapp, Authentication authentication) {
		ReportUserLogin userLogin = new ReportUserLogin();
		int realPage = page - 1;
		String startDate = (String) mapp.get("startDate");
		String endDate = (String) mapp.get("endDate");
       
		if (startDate.equalsIgnoreCase("minDate")) {
			mapp.put("startDate", "1900/01/01");
		} else {
			try {
				mapp.put("startDate", parseStringToOtherStringFormat(startDate, "dd/mm/yyyy", "yyyy/mm/dd"));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (endDate.equalsIgnoreCase("maxDate")) {
			mapp.put("endDate", "3000/12/01");
		} else {
			try {
				mapp.put("endDate", parseStringToOtherStringFormat(endDate, "dd/mm/yyyy", "yyyy/mm/dd"));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		Map<String, String> mapper = getMyRequestToken(authentication);
		HttpRestResponse httpRestResponse = wsBody(wmsParentalWrapper + "/reportUserLoginCtl/count-report/" + role
				+ "?page=" + realPage + "&size=" + COUNTER_PAGING
		// +"&sort=logTglFormatted,desc"
				, mapp, HttpMethod.POST, mapper);
		try {
			userLogin = mapperJsonToSingleDto(httpRestResponse.getBody(), ReportUserLogin.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return userLogin;
	}

	@PostMapping("/new-retrieve-report/{role}")
	public Map<String, Object> getAllPageRetrieveReport(@PathVariable("role") String role,
			@RequestParam("page") int page, @RequestBody Map<String, Object> mapp, HttpSession httpSession,
			Authentication authentication) {
		Map<String, String> mapper = getMyRequestToken(authentication);
		int realPage = page - 1;

		String startDate = (String) mapp.get("startDate");
		String endDate = (String) mapp.get("endDate");
		String search = (String) mapp.get("search");
		mapp.put("search", search);
		if (startDate.equalsIgnoreCase("minDate")) {
			mapp.put("startDate", "1900/01/01");
		} else {
			try {
				mapp.put("startDate", parseStringToOtherStringFormat(startDate, "dd/mm/yyyy", "yyyy/mm/dd"));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (endDate.equalsIgnoreCase("maxDate")) {
			mapp.put("endDate", "3000/12/01");
		} else {
			try {
				mapp.put("endDate", parseStringToOtherStringFormat(endDate, "dd/mm/yyyy", "yyyy/mm/dd"));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		httpSession.setAttribute("mapp", mapp);
		HttpRestResponse httpRestResponse = wsBody(wmsParentalWrapper + "/reportUserLoginCtl/select-all-report/" + role
				+ "?page=" + realPage + "&size=" + COUNTER_PAGING
		// +"&sort=logTglFormatted,desc"
				, mapp, HttpMethod.POST, mapper);
		Map<String, Object> map = mapperJsonToHashMap(httpRestResponse.getBody());

		System.err.println("mapp nya : " + new Gson().toJson(map));
		return map;
	}

	@GetMapping("/download-report/{role}/{typeFile}")
	public ResponseEntity<byte[]> getAllPageRetrieveReport(@PathVariable("typeFile") String typeFile,
			@PathVariable("role") String role, Authentication authentication, HttpSession httpSession)
			throws Exception {
		Map<String, String> mapper = getMyRequestToken(authentication);
		@SuppressWarnings("unchecked")
		Map<String, Object> mapp = (Map<String, Object>) httpSession.getAttribute("mapp");

		HttpRestResponse httpRestResponse = wsBody(
				wmsParentalWrapper + "/reportUserLoginCtl/select-all-report-list/" + role, mapp, HttpMethod.POST,
				mapper);

		List<Object[]> obj = new ArrayList<>();

		try {
			obj = mapperJsonToListDto(httpRestResponse.getBody(), Object[].class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		HttpHeaders responseHeaders = new HttpHeaders();
		byte[] bytearray = null;
		if (typeFile.equalsIgnoreCase("csv")) {
			bytearray = downloadCsv(obj, role);
			responseHeaders.setContentType(MediaType.parseMediaType("text/plain; charset=utf-8"));
			responseHeaders.set("Content-Disposition", "attachment");
			if(role.equalsIgnoreCase("admin")) {
				responseHeaders.setContentDispositionFormData("ListUserLoginEsbnWeb", "ListUserLoginEsbnWeb");
			}else if (role.equalsIgnoreCase("nasabah")) {
				responseHeaders.setContentDispositionFormData("ListNasabahLoginEsbnWeb", "ListNasabahLoginEsbnWeb");
			}
			
		} else if (typeFile.equalsIgnoreCase("excel")) {
			bytearray = createDataExcel(obj, role);
			responseHeaders.setContentType(MediaType.parseMediaType("application/vnd.ms-excel"));
			responseHeaders.set("Content-Disposition", "attachment");
			if(role.equalsIgnoreCase("admin")) {
				responseHeaders.setContentDispositionFormData("ListUserLoginEsbnWeb.xlsx", "ListUserLoginEsbnWeb.xlsx");
			}else if (role.equalsIgnoreCase("nasabah")) {
				responseHeaders.setContentDispositionFormData("ListNasabahLoginEsbnWeb.xlsx", "ListNasabahLoginEsbnWeb.xlsx");
			}
		} else if (typeFile.equalsIgnoreCase("pdf")) {
			bytearray = createDataPdf(obj, role);
			responseHeaders.setContentType(MediaType.parseMediaType("application/pdf"));
			responseHeaders.set("Content-Disposition", "attachment");
			if(role.equalsIgnoreCase("admin")) {
				responseHeaders.setContentDispositionFormData("ListUserLoginEsbnWeb.pdf", "ListUserLoginEsbnWeb.pdf");
			}else if (role.equalsIgnoreCase("nasabah")) {
				responseHeaders.setContentDispositionFormData("ListNasabahLoginEsbnWeb.pdf", "ListNasabahLoginEsbnWeb.pdf");
			}
		}

		
		
		responseHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		return new ResponseEntity<byte[]>(bytearray, responseHeaders, HttpStatus.OK);
	}

	private byte[] downloadCsv(List<Object[]> obj, String role) throws Exception {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(baos));

		CSVPrinter csvPrinter = null;

		if (role.equalsIgnoreCase("admin")) {
			csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader("User Id", "Role", "Tanggal Login"));
			for (Object[] sess : obj) {
				TblEsbnLoginLog esbnLoginLog = new TblEsbnLoginLog();
				esbnLoginLog = mapperJsonToSingleDto(new Gson().toJson(sess[0]), TblEsbnLoginLog.class);
				String objs = (String) sess[1];
				csvPrinter.printRecord(esbnLoginLog.getLogUserId(), esbnLoginLog.getLogUserTypeCodeName(), objs);
			}

			csvPrinter.flush();
		} else if (role.equalsIgnoreCase("nasabah")) {
			csvPrinter = new CSVPrinter(writer,
					CSVFormat.DEFAULT.withHeader("Nomor Cif", "Nama Nasabah", "Tanggal Login"));
			for (Object[] sess : obj) {
				TblEsbnLoginLog esbnLoginLog = new TblEsbnLoginLog();
				esbnLoginLog = mapperJsonToSingleDto(new Gson().toJson(sess[0]), TblEsbnLoginLog.class);
				String objs = (String) sess[1];
				csvPrinter.printRecord(objs, esbnLoginLog.getLogUserName(), esbnLoginLog.getLogTglFormatted());
			}

			csvPrinter.flush();
		}

		return baos.toByteArray();
	}

	private byte[] createDataExcel(List<Object[]> obj, String role) throws Exception {
		XSSFWorkbook workbook = new XSSFWorkbook();

		Sheet sheet = workbook.createSheet("List User Login");

		Font headerFont = workbook.createFont();
		// headerFont.setBold
		headerFont.setFontHeightInPoints((short) 9);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		// headerCellStyle.set
		// Create a Row
		Row headerRow = sheet.createRow(0);

		Cell cell1 = headerRow.createCell(0);
		if (role.equalsIgnoreCase("admin")) {
			cell1.setCellValue("User Id");
		} else if (role.equalsIgnoreCase("nasabah")) {
			cell1.setCellValue("Nomor Cif");
		}
		cell1.setCellStyle(headerCellStyle);

		Cell cell2 = headerRow.createCell(1);
		if (role.equalsIgnoreCase("admin")) {
			cell2.setCellValue("Role");
		} else if (role.equalsIgnoreCase("nasabah")) {
			cell2.setCellValue("Nama Nasabah");
		}
		cell2.setCellStyle(headerCellStyle);

		Cell cell3 = headerRow.createCell(2);
		cell3.setCellValue("Tanggal Login");
		cell3.setCellStyle(headerCellStyle);

		int i = 1;
		for (Object[] sess : obj) {
			TblEsbnLoginLog esbnLoginLog = new TblEsbnLoginLog();
			esbnLoginLog = mapperJsonToSingleDto(new Gson().toJson(sess[0]), TblEsbnLoginLog.class);
			String objs = (String) sess[1];
			Row armpit = sheet.createRow(i);
			Cell celll1 = armpit.createCell(0);
			if (role.equalsIgnoreCase("admin")) {
				celll1.setCellValue(esbnLoginLog.getLogUserId());
			} else if (role.equalsIgnoreCase("nasabah")) {
				celll1.setCellValue(objs);
			}
			celll1.setCellStyle(headerCellStyle);

			if (role.equalsIgnoreCase("admin")) {
				Cell celll2 = armpit.createCell(1);
				celll2.setCellValue(esbnLoginLog.getLogUserTypeCodeName());
				celll2.setCellStyle(headerCellStyle);
			} else {
				Cell celll2 = armpit.createCell(1);
				celll2.setCellValue(esbnLoginLog.getLogUserName());
				celll2.setCellStyle(headerCellStyle);
			}

			Cell celll3 = armpit.createCell(2);
			celll3.setCellValue(esbnLoginLog.getLogTglFormatted());
			celll3.setCellStyle(headerCellStyle);

			i++;
		}
		// Create cells
//	        for(int i = 0; i < columns.length; i++) {
//	            Cell cell = headerRow.createCell(i);
//	            cell.setCellValue(columns[i]);
//	            cell.setCellStyle(headerCellStyle);
//	        }

		sheet.autoSizeColumn(0);
		sheet.autoSizeColumn(1);
		sheet.autoSizeColumn(2);
		sheet.autoSizeColumn(3);
		sheet.autoSizeColumn(4);
		sheet.autoSizeColumn(5);
		sheet.autoSizeColumn(6);
		sheet.autoSizeColumn(7);
		sheet.autoSizeColumn(8);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		workbook.write(bos);
		return bos.toByteArray();
	}

	public byte[] createDataPdf(List<Object[]> obj, String role) throws Exception {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Document document = new Document(PageSize.A4.rotate());
		PdfWriter writer = PdfWriter.getInstance(document, baos);
		writer.setViewerPreferences(getViewerPreferences());
		document.open();

		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.setWidths(new float[] { 0.30f, 0.30f, 0.30f });

		BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
		com.lowagie.text.Font fBig = new com.lowagie.text.Font(bf, 10, com.lowagie.text.Font.BOLD);
		com.lowagie.text.Font fMed = new com.lowagie.text.Font(bf, 10, com.lowagie.text.Font.NORMAL);

		// Phrase phrase1 = new Phrase("Nomor", fBig);

		if (role.equalsIgnoreCase("admin")) {
			PdfPCell c1 = new PdfPCell(new Phrase("User Id", fBig));
			c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1);
		} else if (role.equalsIgnoreCase("nasabah")) {
			PdfPCell c1 = new PdfPCell(new Phrase("Nomor Cif", fBig));
			c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c1.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c1);
		}

		if (role.equalsIgnoreCase("admin")) {
			PdfPCell c2 = new PdfPCell(new Phrase("Role", fBig));
			c2.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c2.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c2);
		} else if (role.equalsIgnoreCase("nasabah")) {
			PdfPCell c2 = new PdfPCell(new Phrase("Nama Nasabah", fBig));
			c2.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c2.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c2);
		}

		PdfPCell c3 = new PdfPCell(new Phrase("Tanggal Login", fBig));
		c3.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c3.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c3);

		for (Object[] sess : obj) {
			TblEsbnLoginLog esbnLoginLog = new TblEsbnLoginLog();
			esbnLoginLog = mapperJsonToSingleDto(new Gson().toJson(sess[0]), TblEsbnLoginLog.class);
			String objs = (String) sess[1];

			if (role.equalsIgnoreCase("admin")) {
				PdfPCell c4 = new PdfPCell(new Phrase(esbnLoginLog.getLogUserId(), fMed));
				c4.setVerticalAlignment(Element.ALIGN_MIDDLE);
				c4.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(c4);
			} else if (role.equalsIgnoreCase("nasabah")) {
				PdfPCell c4 = new PdfPCell(new Phrase(objs, fMed));
				c4.setVerticalAlignment(Element.ALIGN_MIDDLE);
				c4.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(c4);
			}

			if (role.equalsIgnoreCase("admin")) {
				PdfPCell c5 = new PdfPCell(new Phrase(esbnLoginLog.getLogUserTypeCodeName(), fMed));
				c5.setVerticalAlignment(Element.ALIGN_MIDDLE);
				c5.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(c5);
			} else if (role.equalsIgnoreCase("nasabah")) {
				PdfPCell c5 = new PdfPCell(new Phrase(esbnLoginLog.getLogUserName(), fMed));
				c5.setVerticalAlignment(Element.ALIGN_MIDDLE);
				c5.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(c5);
			}

			if (role.equalsIgnoreCase("admin")) {
				PdfPCell c6 = new PdfPCell(new Phrase(esbnLoginLog.getLogTglFormatted(), fMed));
				c6.setVerticalAlignment(Element.ALIGN_MIDDLE);
				c6.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(c6);
			} else if (role.equalsIgnoreCase("nasabah")) {
				PdfPCell c6 = new PdfPCell(new Phrase(esbnLoginLog.getLogTglFormatted(), fMed));
				c6.setVerticalAlignment(Element.ALIGN_MIDDLE);
				c6.setHorizontalAlignment(Element.ALIGN_CENTER);
				table.addCell(c6);
			}

		}

		document.add(table);
		document.close();

		return baos.toByteArray();
	}

	protected int getViewerPreferences() {
		return PdfWriter.ALLOW_PRINTING | PdfWriter.PageLayoutSinglePage;
	}

}
