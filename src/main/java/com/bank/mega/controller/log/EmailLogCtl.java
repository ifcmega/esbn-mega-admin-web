package com.bank.mega.controller.log;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bank.mega.service.BaseService;

@Controller
public class EmailLogCtl extends BaseService {
	@RequestMapping("/email-log")
	public String emailLogMonitoring(Model model, Authentication authentication, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		informationHeader(model, authentication);
		session.removeAttribute("fromDates");
		session.removeAttribute("toDates");
		session.removeAttribute("statuss");
		session.removeAttribute("koloms"); 
		session.removeAttribute("searchs");
		session.removeAttribute("subjects");
		return checkSessionValidity("log/email-log", authentication, request, response);
	}

}
