package com.bank.mega.controller.log;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bank.mega.service.BaseService;

@Controller
public class SmsLogCtl extends BaseService {
	@RequestMapping("/sms-log")
	public String smsLogMonitoring(Model model, Authentication authentication, HttpServletRequest request,
			HttpServletResponse response,HttpSession session) {
		informationHeader(model, authentication);
		session.removeAttribute("fromDatess");
		session.removeAttribute("toDatess");
		session.removeAttribute("statusss");
		session.removeAttribute("kolomss"); 
		session.removeAttribute("searchss");
		return checkSessionValidity("log/sms-log", authentication, request, response);
	}
}
