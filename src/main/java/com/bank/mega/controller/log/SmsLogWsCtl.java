package com.bank.mega.controller.log;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.bean.log.EmailLogBean;
import com.bank.mega.bean.log.EmailLogBodyRequest;
import com.bank.mega.bean.log.RequestBodyEmailLog;
import com.bank.mega.bean.log.SmsLogBean;
import com.bank.mega.bean.log.SmsLogBeanListPaging;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.service.BaseService;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

@RestController
@RequestMapping("/smsLogWsCtl")
public class SmsLogWsCtl extends BaseService {
	@PostMapping("get-sms-log")
	public SmsLogBeanListPaging getDataSmsLog(@RequestBody EmailLogBodyRequest req, Authentication auth, HttpSession session)
			throws ParseException {
		TokenWrapper tokenWrapper = getMyToken(auth);
		SmsLogBeanListPaging listPaging = new SmsLogBeanListPaging();
		RequestBodyEmailLog bodyEmailLog = new RequestBodyEmailLog();
		Map<String, Object> map = new HashMap<>();

		if(req.getFromDate() == null || "".equalsIgnoreCase(req.getFromDate())) {
			listPaging.setIsError(true);
			listPaging.setReason("Tanggal 'From' tidak boleh kosong");
			return listPaging;
		}
		
		if(req.getToDate() == null || "".equalsIgnoreCase(req.getToDate())) {
			listPaging.setIsError(true);
			listPaging.setReason("Tanggal 'To' tidak boleh kosong");
			return listPaging;
		}
		
		if (req.getFromDate().equalsIgnoreCase("fromDate")) {
			req.setFromDate("01/01/1900");
		}
		
		if (req.getToDate().equalsIgnoreCase("toDate")) {
			req.setToDate("01/12/3000");
		}
		
		map.put("fromDate", changeDate(req.getFromDate()));
		map.put("toDate", changeDate(req.getToDate()));
		map.put("status", req.getStatus());
		if ((req.getSearchKolom() != null && req.getSearch() != null)
				|| (req.getSearchKolom() != "" && req.getSearch() != "")) {
			map.put(req.getSearchKolom(), req.getSearch());
		}

		bodyEmailLog.setSize(COUNTER_PAGING);
		bodyEmailLog.setPage(req.getPage());
		bodyEmailLog.setSearch(req.getSearch());
		bodyEmailLog.setSearchKolom(req.getSearchKolom());
		bodyEmailLog.setSortBy(req.getSortBy());
		bodyEmailLog.setSortCondition(req.getSortCondition());
		bodyEmailLog.setFiltering(map);

		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		HttpRestResponse httpRestResponse = wsBody(wmsParentalWrapper + "/smsLogCtl/sms-log/page?page="
				+ bodyEmailLog.getPage() + "&size=" + bodyEmailLog.getSize() + "&sort=" + bodyEmailLog.getSortBy() + ","
				+ bodyEmailLog.getSortCondition(), bodyEmailLog, HttpMethod.POST, headerMap);
		System.out.println(new Gson().toJson(bodyEmailLog));
		System.out.println(new Gson().toJson(httpRestResponse));

		List<SmsLogBean> listSmsLogBean = new ArrayList<>();

		try {
			listPaging = mapperJsonToSingleDto(httpRestResponse.getBody(), SmsLogBeanListPaging.class);
			for (SmsLogBean logBean : listPaging.getContent()) {
				logBean.setLogTglWaktuString(parseDateToString(logBean.getLogTglWaktu(), CONTENT_FORMAT_DATE));
				listSmsLogBean.add(logBean);
			}
			listPaging.setContent(listSmsLogBean);
			
			session.setAttribute("fromDatess", changeDate(req.getFromDate()));
			session.setAttribute("toDatess", changeDate(req.getToDate()));
			session.setAttribute("statusss", req.getStatus());
			session.setAttribute("kolomss", req.getSearchKolom());
			session.setAttribute("searchss", req.getSearch());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return listPaging;
	}

	
	public List<SmsLogBean> getSmsLogList(Authentication auth, HttpSession session){
		List<SmsLogBean> listSmsLog =  new ArrayList<>();
		List<SmsLogBean> listSmsLogFinal =  new ArrayList<>();
		
		TokenWrapper tokenWrapper = getMyToken(auth);
		Map<String, Object> mapObj = new HashMap<>();
		
		
		String fromDate = (String) session.getAttribute("fromDatess");
		String toDate = (String) session.getAttribute("toDatess");
		String status = (String) session.getAttribute("statusss");
		String tujuan = null;
		String subject = null;
		String user = null;
		String resend = null;
		String id = null;
		String tgl = null;
		String pesan = null;
		String statuslog = null;
		String kolom = (String) session.getAttribute("kolomss");
		String search = (String) session.getAttribute("searchss");
		
		if (Strings.isNullOrEmpty(kolom) && Strings.isNullOrEmpty(search)) {
			tujuan = "";
			user = "";
			resend = "";
			statuslog = "";
			pesan = "";
			id = "";
			tgl = "";
		} else {
			if (kolom.equalsIgnoreCase("tujuan")) {
				if (Strings.isNullOrEmpty(search)) {
					tujuan = "";
				} else {
					tujuan = search;
				}
			}else if (kolom.equalsIgnoreCase("user")) {
				if (Strings.isNullOrEmpty(search)) {
					user = "";
				} else {
					user = search;
				}
			}else if (kolom.equalsIgnoreCase("resend")) {
				if (Strings.isNullOrEmpty(search)) {
					resend = "";
				} else {
					resend = search;
				}
			}else if (kolom.equalsIgnoreCase("statuslog")) {
				if (Strings.isNullOrEmpty(search)) {
					statuslog = "";
				} else {
					statuslog = search;
				}
			}else if (kolom.equalsIgnoreCase("pesan")) {
				if (Strings.isNullOrEmpty(search)) {
					pesan = "";
				} else {
					pesan = search;
				}
			}else if (kolom.equalsIgnoreCase("id")) {
				if (Strings.isNullOrEmpty(search)) {
					id = "";
				} else {
					id = search;
				}
			}else if (kolom.equalsIgnoreCase("tgl")) {
				if (Strings.isNullOrEmpty(search)) {
					tgl = "";
				} else {
					tgl = search;
				}
			}
		}
		
//		
//		if (!Strings.isNullOrEmpty((String) session.getAttribute("searchs"))
//				&& Strings.isNullOrEmpty((String) session.getAttribute("koloms"))) {
//			if (session.getAttribute("koloms").equals("tujuan")
//					&& !Strings.isNullOrEmpty((String) session.getAttribute("searchs"))) {
//				tujuan = (String) session.getAttribute("searchs");
//			}  else if (session.getAttribute("koloms").equals("user")
//					&& !Strings.isNullOrEmpty((String) session.getAttribute("searchs"))) {
//				user = (String) session.getAttribute("searchs");
//			} else if (session.getAttribute("koloms").equals("subject")
//					&& !Strings.isNullOrEmpty((String) session.getAttribute("searchs"))) {
//				subject = (String) session.getAttribute("searchs");
//			} else if (session.getAttribute("koloms").equals("resend")
//					&& !Strings.isNullOrEmpty((String) session.getAttribute("searchs"))) {
//				resend = (String) session.getAttribute("searchs");
//			} else if (session.getAttribute("koloms").equals("statuslog")
//					&& !Strings.isNullOrEmpty((String) session.getAttribute("searchs"))) {
//				statuslog = (String) session.getAttribute("searchs");
//			} else if (session.getAttribute("koloms").equals("id")
//					&& !Strings.isNullOrEmpty((String) session.getAttribute("searchs"))) {
//				id = (String) session.getAttribute("searchs");
//			} else if (session.getAttribute("koloms").equals("tgl")
//					&& !Strings.isNullOrEmpty((String) session.getAttribute("searchs"))) {
//				tgl = (String) session.getAttribute("searchs");
//			} else if (session.getAttribute("koloms").equals("pesan")
//					&& !Strings.isNullOrEmpty((String) session.getAttribute("searchs"))) {
//				tgl = (String) session.getAttribute("searchs");
//			} else {
//				tujuan = "";
//				user = "";
//				resend = "";
//				pesan = "";
//				id = "";
//				statuslog="";
//				tgl = "";
//			}
//		} else {
//			tujuan = "";
//			user = "";
//			resend = "";
//			statuslog = "";
//			pesan = "";
//			id = "";
//			tgl = "";
//		}

		mapObj.put("fromDate", fromDate);
		mapObj.put("toDate", toDate);
		mapObj.put("status", status);
		mapObj.put("tujuan", tujuan);
		mapObj.put("subject", subject);
		mapObj.put("user", user);
		mapObj.put("resend", resend);
		mapObj.put("id", id);
		mapObj.put("tgl", tgl);
		mapObj.put("pesan", pesan);
		mapObj.put("statuslog", statuslog);

		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		HttpRestResponse httpRestResponse = wsBody(wmsParentalWrapper + "/smsLogCtl/sms-log/list", mapObj,
				HttpMethod.POST, headerMap);
		
		try {
			listSmsLog = mapperJsonToListDto(httpRestResponse.getBody(), SmsLogBean.class);
			
			for (SmsLogBean bean : listSmsLog) {
				bean.setLogId(bean.getLogId());
				bean.setLogPesan(bean.getLogPesan());
				bean.setLogResend(bean.getLogResend());
				bean.setLogStatus(bean.getLogStatus());
				bean.setLogSubject(bean.getLogSubject());
				bean.setLogTglWaktu(bean.getLogTglWaktu());
				bean.setLogTglWaktuString(parseDateToStringV2(bean.getLogTglWaktu()));;
				bean.setLogTujuan(bean.getLogTujuan());
				bean.setLogUser(bean.getLogUser());
				listSmsLogFinal.add(bean);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return listSmsLogFinal;
	}
	
	
	@RequestMapping(value = "/exportSmsLogExcel", method = RequestMethod.GET)
	public ResponseEntity<byte[]> exportSmsLogExcel(HttpSession session, Authentication auth) throws IOException {

		List<SmsLogBean> beans = getSmsLogList(auth, session);

		byte[] array = createDataExcel(beans);
		ResponseEntity<byte[]> rsp = new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setContentType(MediaType.parseMediaType("application/vnd.ms-excel"));
		responseHeaders.set("Content-Disposition", "attachment");
		responseHeaders.setContentDispositionFormData("smsLog.xlsx", "smsLog.xlsx");
		responseHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		rsp = new ResponseEntity<byte[]>(array, responseHeaders, HttpStatus.OK);
	
		return rsp;
	}
	
	
	
	
	@RequestMapping(value = "/exportSmsLogCsv", method = RequestMethod.GET)
	public ResponseEntity<byte[]> exportSmsLogCsv(HttpSession session, Authentication auth) throws IOException {

		List<SmsLogBean> beans = getSmsLogList(auth, session);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(baos));

		CSVPrinter csvPrinter = new CSVPrinter(writer,
				CSVFormat.DEFAULT.withHeader("Nomor", "Tanggal Pengiriman", "Nomor Telepon", "Subject", "Status", "User"));

		int i = 1;

		for (SmsLogBean sms : beans) {
			csvPrinter.printRecord(sms.getLogId(), sms.getLogTglWaktuString(), sms.getLogTujuan(),
					sms.getLogSubject(), sms.getLogStatus(), sms.getLogUser());
			
			i++;
		}

		csvPrinter.flush();
		
		ResponseEntity<byte[]> rsp = new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setContentType(MediaType.parseMediaType("text/plain; charset=utf-8"));
		responseHeaders.set("Content-Disposition", "attachment");
		responseHeaders.setContentDispositionFormData("smsLog", "smsLog");
		responseHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		rsp = new ResponseEntity<byte[]>(baos.toByteArray(), responseHeaders, HttpStatus.OK);

		return rsp;
	}
	
	
	protected int getViewerPreferences() {
		return PdfWriter.ALLOW_PRINTING | PdfWriter.PageLayoutSinglePage;
	}
	
	
	
	@RequestMapping(value = "/exportSmsLogPdf", method = RequestMethod.GET)
	public ResponseEntity<byte[]> exportSmsLogPdf(HttpSession session, Authentication auth)
			throws IOException, DocumentException {

		List<SmsLogBean> beans = getSmsLogList(auth, session);

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Document document = new Document(PageSize.A4.rotate());
		PdfWriter writer = PdfWriter.getInstance(document, baos);
		writer.setViewerPreferences(getViewerPreferences());
		document.open();
		
		PdfPTable table = new PdfPTable(6);
		table.setWidthPercentage(100);
		table.setWidths(new float[]{0.15f,0.27f,0.24f,0.26f,0.18f,0.24f});
		
		BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
		com.lowagie.text.Font fBig = new com.lowagie.text.Font(bf, 10, com.lowagie.text.Font.BOLD);
		com.lowagie.text.Font fMed = new com.lowagie.text.Font(bf, 10, com.lowagie.text.Font.NORMAL);
		
		//Phrase phrase1 = new Phrase("Nomor", fBig);
		PdfPCell c1 = new PdfPCell(new Phrase("Nomor",fBig));
		c1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);
		
		PdfPCell c2 = new PdfPCell(new Phrase("Tanggal Pengiriman",fBig));
		c2.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c2.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c2);
		
		PdfPCell c3 = new PdfPCell(new Phrase("Nomor Telepon",fBig));
		c3.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c3.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c3);

		PdfPCell c4 = new PdfPCell(new Phrase("Subject",fBig));
		c4.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c4.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c4);
		
		PdfPCell c5 = new PdfPCell(new Phrase("Status",fBig));
		c5.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c5.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c5);
		
		PdfPCell c6 = new PdfPCell(new Phrase("User",fBig));
		c6.setVerticalAlignment(Element.ALIGN_MIDDLE);
		c6.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c6);
		

		for (SmsLogBean email : beans) {
			
			PdfPCell c7 = new PdfPCell(new Phrase(Long.toString(email.getLogId()),fMed));
			c7.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c7.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c7);
			
			PdfPCell c8 = new PdfPCell(new Phrase(email.getLogTglWaktuString(),fMed));
			c8.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c8.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c8);
			
			PdfPCell c9 = new PdfPCell(new Phrase(email.getLogTujuan(),fMed));
			c9.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c9.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c9);
			
			PdfPCell c10 = new PdfPCell(new Phrase(email.getLogSubject(),fMed));
			c10.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c10.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c10);
			
			PdfPCell c11 = new PdfPCell(new Phrase(email.getLogStatus(),fMed));
			c11.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c11.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c11);
			
			PdfPCell c12 = new PdfPCell(new Phrase(email.getLogUser(),fMed));
			c12.setVerticalAlignment(Element.ALIGN_MIDDLE);
			c12.setHorizontalAlignment(Element.ALIGN_CENTER);
			table.addCell(c12);
		}

		document.add(table);
		document.close();
		ResponseEntity<byte[]> rsp = new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setContentType(MediaType.parseMediaType("application/pdf"));
		responseHeaders.set("Content-Disposition", "attachment");
		responseHeaders.setContentDispositionFormData("smsLog.pdf", "smsLog.pdf");
		responseHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		rsp = new ResponseEntity<byte[]>(baos.toByteArray(), responseHeaders, HttpStatus.OK);

		return rsp;
	}
	
	
	
	private byte[] createDataExcel(List<SmsLogBean> beans) throws IOException {
		XSSFWorkbook workbook = new XSSFWorkbook();

		Sheet sheet = workbook.createSheet("Export Email Log");

		Font headerFont = workbook.createFont();
		// headerFont.setBold
		headerFont.setFontHeightInPoints((short) 9);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		// Create a Row
		Row headerRow = sheet.createRow(0);

		Cell cell1 = headerRow.createCell(0);
		cell1.setCellValue("Nomor");
		cell1.setCellStyle(headerCellStyle);

		Cell cell2 = headerRow.createCell(1);
		cell2.setCellValue("Tanggal Pengiriman");
		cell2.setCellStyle(headerCellStyle);

		Cell cell3 = headerRow.createCell(2);
		cell3.setCellValue("Nomor Telepon");
		cell3.setCellStyle(headerCellStyle);

		Cell cell4 = headerRow.createCell(3);
		cell4.setCellValue("Subject");
		cell4.setCellStyle(headerCellStyle);

		Cell cell5 = headerRow.createCell(4);
		cell5.setCellValue("Status");
		cell5.setCellStyle(headerCellStyle);

		Cell cell6 = headerRow.createCell(5);
		cell6.setCellValue("User");
		cell6.setCellStyle(headerCellStyle);

		int i = 1;
		for (SmsLogBean cell : beans) {
			Row armpit = sheet.createRow(i);
			Cell celll1 = armpit.createCell(0);
			celll1.setCellValue(cell.getLogId());
			celll1.setCellStyle(headerCellStyle);

			Cell celll2 = armpit.createCell(1);
			celll2.setCellValue(cell.getLogTglWaktuString());
			celll2.setCellStyle(headerCellStyle);

			Cell celll3 = armpit.createCell(2);
			celll3.setCellValue(cell.getLogTujuan());
			celll3.setCellStyle(headerCellStyle);

			Cell celll4 = armpit.createCell(3);
			celll4.setCellValue(cell.getLogSubject());
			celll4.setCellStyle(headerCellStyle);

			Cell celll5 = armpit.createCell(4);
			celll5.setCellValue(cell.getLogStatus());
			celll5.setCellStyle(headerCellStyle);

			Cell celll6 = armpit.createCell(5);
			celll6.setCellValue(cell.getLogUser());
			celll6.setCellStyle(headerCellStyle);

			i++;
		}

		sheet.autoSizeColumn(0);
		sheet.autoSizeColumn(1);
		sheet.autoSizeColumn(2);
		sheet.autoSizeColumn(3);
		sheet.autoSizeColumn(4);
		sheet.autoSizeColumn(5);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		workbook.write(bos);
		return bos.toByteArray();
	}
	
	
	
	public String changeDate(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyyMMdd");
		return sdf2.format(sdf.parse(date));
	}
}
