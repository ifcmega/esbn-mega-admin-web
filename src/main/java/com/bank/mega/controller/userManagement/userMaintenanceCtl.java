package com.bank.mega.controller.userManagement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bank.mega.service.BaseService;

@Controller
public class userMaintenanceCtl extends BaseService{

	@RequestMapping("/userList")
	public String userListCaller(Model model, Authentication authentication, HttpServletRequest request, HttpServletResponse response) {
		informationHeader(model, authentication);
		
		return checkSessionValidity("userManagement/userList",authentication, 
				request, response);
	}
	
	@RequestMapping("/uploadUser")
	public String uploadUserCaller(Model model,Authentication authentication, HttpServletRequest request, HttpServletResponse response) {
		informationHeader(model, authentication);
		return checkSessionValidity("userManagement/uploadUser",authentication, 
				request, response);
	}
	
	@RequestMapping("/userNasabah")
	public String userNasabahCaller(Model model,Authentication authentication, HttpServletRequest request, HttpServletResponse response) {
		informationHeader(model, authentication);
		return checkSessionValidity("userManagement/userNasabah",authentication, 
				request, response);
	}
}
