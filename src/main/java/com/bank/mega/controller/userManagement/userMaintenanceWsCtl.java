package com.bank.mega.controller.userManagement;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bank.mega.bean.admin.PageRequestBean;
import com.bank.mega.bean.admin.PageRequestResult;
import com.bank.mega.bean.admin.PagingUserListAdmin;
import com.bank.mega.bean.admin.ResultUploadDataUserListBean;
import com.bank.mega.bean.admin.TblAdminEsbnBean;
import com.bank.mega.bean.admin.TblAdminEsbnBeanUpload;
import com.bank.mega.bean.admin.UploadMenuPagingBean;
import com.bank.mega.bean.admin.UserManagementUserListBean;
import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.bean.principal.BasePrincipalSessions;
import com.bank.mega.security.CustomUserService;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.security.WsResponse;
import com.bank.mega.service.BaseService;
import com.bank.mega.validator.EmailValidator;
import com.google.gson.Gson;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

@RestController
@RequestMapping("/userMaintenanceWsCtl")
public class userMaintenanceWsCtl extends BaseService {

	@GetMapping("/check-validity/{userId}")
	private Map<String, Object> checkValidityUser(@PathVariable("userId") String userId,
			Authentication authentication) {
		Map<String, String> headerMap = getMyRequestToken(authentication);
		HttpRestResponse httpRestResponse = wsBody(
				wmsParentalWrapper + "/userManagementCtl/check-validity-user/" + userId, null, HttpMethod.GET,
				headerMap);
		Map<String, Object> map = new HashMap<>();
		map = mapperJsonToHashMap(httpRestResponse.getBody());
		return map;
	}

	@PostMapping("/check-validity-email")
	private Map<String, Object> checkValidityUserEmail(@RequestBody String email, Authentication authentication) {
		Map<String, Object> map = new HashMap<>();
		System.err.println("email : " + email);
		if (EmailValidator.isValid(email)) {
			map.put("isValid", true);

		} else {
			map.put("isValid", false);
			map.put("reason", "Format Email Masih Salah");
		}
		return map;
	}

	@PostMapping("/initialize-data/{role}")
	public PagingUserListAdmin getAllListUserManagement(@PathVariable("role") String role,
			@RequestBody PageRequestBean pageRequestBean, Authentication authentication, HttpSession session) {

		TokenWrapper tokenWrapper = getMyToken(authentication);
		pageRequestBean.setSize(COUNTER_PAGING);
		System.err.println("pagination : " + new Gson().toJson(pageRequestBean));
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		HttpRestResponse httpRestResponse = wsBody(wmsParentalWrapper + "/userManagementCtl/inquiry/" + role,
				pageRequestBean, HttpMethod.POST, headerMap);
		PagingUserListAdmin map = new PagingUserListAdmin();
		try {
			map = mapperJsonToSingleDto(httpRestResponse.getBody(), PagingUserListAdmin.class);
			List<UserManagementUserListBean> pagingUserListAdmins = new ArrayList<>();
			for (UserManagementUserListBean pagingUserListAdmin : map.getContent()) {
				pagingUserListAdmin.setCreatedDateString(
						parseDateToString(pagingUserListAdmin.getCreatedDate(), CONTENT_FORMAT_DATE));
				pagingUserListAdmin.setIsLogin(isActiveSession(pagingUserListAdmin.getUserId(), authentication));
				if (pagingUserListAdmin.getLastUpdatedDate() != null) {
					pagingUserListAdmin.setLastUpdatedDateString(
							parseDateToString(pagingUserListAdmin.getLastUpdatedDate(), CONTENT_FORMAT_DATE));
				}
				if (pageRequestBean.getSearchKolom() != null) {
					if (pageRequestBean.getSearchKolom().equalsIgnoreCase("isLogin")) {
						if (Boolean.parseBoolean(pageRequestBean.getSearch()) == pagingUserListAdmin.getIsLogin()) {
							pagingUserListAdmins.add(pagingUserListAdmin);
						}
					}else if (pageRequestBean.getSearchKolom().equalsIgnoreCase("nama")) {
						if (pagingUserListAdmin.getNamaLengkap().toUpperCase()
								.contains(pageRequestBean.getSearch().toUpperCase())) {
							pagingUserListAdmins.add(pagingUserListAdmin);
						}
					} else {
						pagingUserListAdmins.add(pagingUserListAdmin);
					}
				}else {
					pagingUserListAdmins.add(pagingUserListAdmin);
				}

			}
			map.setContent(pagingUserListAdmins);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return map;
	}

	@GetMapping("/forcedClosed/{userSid}")
	public void killedSession(@PathVariable(name = "userSid", required = true) String userSid,
			Authentication authentication, HttpServletRequest request, HttpServletResponse response) {
		checkSessionValidity(authentication, request, response);
		for (BasePrincipalSessions element : getAllSessionsGlobal(authentication)) {
			if (element.getPrincipal().getUserName().equalsIgnoreCase(userSid)) {
				System.err
						.println("session yang akan di kill dari id " + userSid + " adalah " + element.getSessionId());
				sessionRegistry.removeSessionInformation(element.getSessionId());
				Map<String, String> hash = getMyRequestToken(authentication);
				HttpRestResponse responsive = wsBody(wmsParentalWrapper + "/session/killedAllThisSession/",
						element.getSessionId(), HttpMethod.POST, hash);
				break;
			}
		}
	}

	@PostMapping("/add-data")
	public Map<String, Object> getAllListUserManagement(@RequestBody TblAdminEsbnBean pageRequestBean,
			Authentication authentication) {
		if (pageRequestBean.getUserSid() == null || pageRequestBean.getUserSid().equals("")) {
			Map<String, Object> map = new HashMap<>();
			map.put("valid", false);
			map.put("reason", "User Id Tidak Boleh Kosong");
			return map;
		} else if (pageRequestBean.getUserName() == null || pageRequestBean.getUserName().equals("")) {
			Map<String, Object> map = new HashMap<>();
			map.put("valid", false);
			map.put("reason", "User Name Tidak Boleh Kosong");
			return map;
		} else if (pageRequestBean.getEmail() == null || pageRequestBean.getUserName().equals("")) {
			Map<String, Object> map = new HashMap<>();
			map.put("valid", false);
			map.put("reason", "Email tidak boleh kosong");
			return map;
		} else if (!EmailValidator.isValid(pageRequestBean.getEmail())) {
			Map<String, Object> map = new HashMap<>();
			map.put("valid", false);
			map.put("reason", "Format Email Salah");
			return map;
		}
		TokenWrapper tokenWrapper = getMyToken(authentication);
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		HttpRestResponse httpRestResponse = wsBody(wmsParentalWrapper + "/userManagementCtl/add-admin", pageRequestBean,
				HttpMethod.POST, headerMap);
		Map<String, Object> map = mapperJsonToHashMap(httpRestResponse.getBody());
		map.put("valid", true);
		return map;
	}

	@RequestMapping(value = "/downloadableFile", method = RequestMethod.GET)
	public ResponseEntity<byte[]> download(Model model, Authentication authentication, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(baos));

		CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader("No.", "User Id", "Nama Lengkap",
				"Email", "isLogin", "Roles", "Enabled", "Locked", "created date"));

		List<UserManagementUserListBean> beans = new ArrayList<>();
		TokenWrapper tokenWrapper = getMyToken(authentication);
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		HttpRestResponse httpRestResponse = wsBody(wmsParentalWrapper + "/userManagementCtl" + "/retrieve-all-value",
				null, HttpMethod.GET, headerMap);

		try {
			beans = mapperJsonToListDto(httpRestResponse.getBody(), UserManagementUserListBean.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int i = 1;
		for (UserManagementUserListBean sess : beans) {
			csvPrinter.printRecord(i, sess.getUserId(), sess.getNamaLengkap(), sess.getEmail(),
					isActiveSession(sess.getUserId(), authentication), sess.getRoles(), sess.getEnabled(),
					sess.getLocked(), sess.getCreatedDate());
			i++;
		}

		csvPrinter.flush();

		ResponseEntity<byte[]> rsp = new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setContentType(MediaType.parseMediaType("text/plain; charset=utf-8"));
		responseHeaders.set("Content-Disposition", "attachment");
		responseHeaders.setContentDispositionFormData("List User Login Esbn Web", "List User Login Esbn Web");
		responseHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		rsp = new ResponseEntity<byte[]>(baos.toByteArray(), responseHeaders, HttpStatus.OK);

		return rsp;
	}

	private byte[] createDataExcel(List<TblAdminEsbnBeanUpload> beans) throws IOException {
		XSSFWorkbook workbook = new XSSFWorkbook();

		Sheet sheet = workbook.createSheet("Rejected");

		Font headerFont = workbook.createFont();
		// headerFont.setBold
		headerFont.setFontHeightInPoints((short) 9);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		// headerCellStyle.set
		// Create a Row
		Row headerRow = sheet.createRow(0);

		Cell cell1 = headerRow.createCell(0);
		cell1.setCellValue("User Id");
		cell1.setCellStyle(headerCellStyle);
		Cell cell2 = headerRow.createCell(1);

		cell2.setCellValue("Nama Lengkap");
		cell2.setCellStyle(headerCellStyle);
		Cell cell3 = headerRow.createCell(2);
		cell3.setCellValue("Email");
		cell3.setCellStyle(headerCellStyle);
		Cell cell4 = headerRow.createCell(3);
		cell4.setCellValue("Is Login");
		cell4.setCellStyle(headerCellStyle);
		Cell cell5 = headerRow.createCell(4);
		cell5.setCellValue("Roles");
		cell5.setCellStyle(headerCellStyle);
		Cell cell6 = headerRow.createCell(5);
		cell6.setCellValue("Enabled");
		cell6.setCellStyle(headerCellStyle);
		Cell cell7 = headerRow.createCell(6);
		cell7.setCellValue("locked");
		cell7.setCellStyle(headerCellStyle);

		Cell cell8 = headerRow.createCell(7);
		cell8.setCellValue("Accepted");
		cell8.setCellStyle(headerCellStyle);

		Cell cell9 = headerRow.createCell(8);
		cell9.setCellValue("Reason");
		cell9.setCellStyle(headerCellStyle);

		int i = 1;
		for (TblAdminEsbnBeanUpload cell : beans) {
			Row armpit = sheet.createRow(i);
			Cell celll1 = armpit.createCell(0);
			celll1.setCellValue(cell.getUserSid());
			celll1.setCellStyle(headerCellStyle);
			Cell celll2 = armpit.createCell(1);
			celll2.setCellValue(cell.getUserName());
			celll2.setCellStyle(headerCellStyle);
			Cell celll3 = armpit.createCell(2);
			celll3.setCellValue(cell.getEmail());
			celll3.setCellStyle(headerCellStyle);
			Cell celll4 = armpit.createCell(3);
			if (cell.getIsLogin() == null) {
				celll4.setCellValue("");
			} else {
				if (cell.getIsLogin().booleanValue()) {
					celll4.setCellValue("Yes");
				} else {
					celll4.setCellValue("No");
				}
			}
			celll4.setCellStyle(headerCellStyle);
			Cell celll5 = armpit.createCell(4);
			celll5.setCellValue(cell.getRoles());
			celll5.setCellStyle(headerCellStyle);
			Cell celll6 = armpit.createCell(5);
			if (cell.getEnabled() == null) {
				celll6.setCellValue("");
			} else {
				if (cell.getEnabled().booleanValue()) {
					celll6.setCellValue("Yes");
				} else {
					celll6.setCellValue("No");
				}
			}

			celll6.setCellStyle(headerCellStyle);
			Cell celll7 = armpit.createCell(6);

			if (cell.getLocked() == null) {
				celll7.setCellValue("");
			} else {
				if (cell.getLocked().booleanValue()) {
					celll7.setCellValue("Yes");
				} else {
					celll7.setCellValue("No");
				}
			}

			celll7.setCellStyle(headerCellStyle);
			Cell celll8 = armpit.createCell(7);
			if (cell.isAccepted()) {
				celll8.setCellValue("Yes");
			} else {
				celll8.setCellValue("No");
			}
			celll8.setCellStyle(headerCellStyle);
			Cell celll9 = armpit.createCell(8);
			celll9.setCellValue(cell.getReasonUnaccepted());
			celll9.setCellStyle(headerCellStyle);
			i++;
		}
		// Create cells
//	        for(int i = 0; i < columns.length; i++) {
//	            Cell cell = headerRow.createCell(i);
//	            cell.setCellValue(columns[i]);
//	            cell.setCellStyle(headerCellStyle);
//	        }

		sheet.autoSizeColumn(0);
		sheet.autoSizeColumn(1);
		sheet.autoSizeColumn(2);
		sheet.autoSizeColumn(3);
		sheet.autoSizeColumn(4);
		sheet.autoSizeColumn(5);
		sheet.autoSizeColumn(6);
		sheet.autoSizeColumn(7);
		sheet.autoSizeColumn(8);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		workbook.write(bos);
		return bos.toByteArray();
	}

	private byte[] createFormatExcel() throws IOException {
		XSSFWorkbook workbook = new XSSFWorkbook();

		Sheet sheet = workbook.createSheet("Format");

		Font headerFont = workbook.createFont();
		// headerFont.setBold
		headerFont.setFontHeightInPoints((short) 9);
		headerFont.setColor(IndexedColors.BLACK.getIndex());

		// Create a CellStyle with the font
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		// headerCellStyle.set
		// Create a Row
		Row headerRow = sheet.createRow(0);

		Cell cell1 = headerRow.createCell(0);
		cell1.setCellValue("User Id");
		cell1.setCellStyle(headerCellStyle);
		Cell cell2 = headerRow.createCell(1);

		cell2.setCellValue("Nama Lengkap");
		cell2.setCellStyle(headerCellStyle);
		Cell cell3 = headerRow.createCell(2);
		cell3.setCellValue("Email");
		cell3.setCellStyle(headerCellStyle);
//		Cell cell4 = headerRow.createCell(3);
//		cell4.setCellValue("Is Login");
//		cell4.setCellStyle(headerCellStyle);
		Cell cell5 = headerRow.createCell(3);
		cell5.setCellValue("Roles");
		cell5.setCellStyle(headerCellStyle);
		Cell cell6 = headerRow.createCell(4);
		cell6.setCellValue("Enabled");
		cell6.setCellStyle(headerCellStyle);
		Cell cell7 = headerRow.createCell(5);
		cell7.setCellValue("locked");
		cell7.setCellStyle(headerCellStyle);

		sheet.autoSizeColumn(0);
		sheet.autoSizeColumn(1);
		sheet.autoSizeColumn(2);
		sheet.autoSizeColumn(3);
		sheet.autoSizeColumn(4);
		sheet.autoSizeColumn(5);
		sheet.autoSizeColumn(6);
		sheet.autoSizeColumn(7);
		sheet.autoSizeColumn(8);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		workbook.write(bos);
		return bos.toByteArray();
	}

	@RequestMapping(value = "/downloadableRejectedFile/{nameFile}", method = RequestMethod.GET)
	public ResponseEntity<byte[]> downloadRejectedFile(
			@PathVariable(name = "nameFile", required = true) String nameFile, HttpSession session) throws IOException {

		@SuppressWarnings("unchecked")
		List<TblAdminEsbnBeanUpload> beans = (List<TblAdminEsbnBeanUpload>) session.getAttribute(nameFile);

		byte[] array = createDataExcel(beans);
		ResponseEntity<byte[]> rsp = new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setContentType(MediaType.parseMediaType("application/vnd.ms-excel"));
		responseHeaders.set("Content-Disposition", "attachment");
		responseHeaders.setContentDispositionFormData("rejected.xlsx", "rejected.xlsx");
		responseHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		rsp = new ResponseEntity<byte[]>(array, responseHeaders, HttpStatus.OK);

		return rsp;
	}

	@RequestMapping(value = "/downloadableFormat", method = RequestMethod.GET)
	public ResponseEntity<byte[]> downloadableFormat(HttpSession session) throws IOException {

		byte[] array = createFormatExcel();
		ResponseEntity<byte[]> rsp = new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);

		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setContentType(MediaType.parseMediaType("application/vnd.ms-excel"));
		responseHeaders.set("Content-Disposition", "attachment");
		responseHeaders.setContentDispositionFormData("Format Upload.xlsx", "Format Upload.xlsx");
		responseHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		rsp = new ResponseEntity<byte[]>(array, responseHeaders, HttpStatus.OK);

		return rsp;
	}

	@PostMapping("/uploadFile")
	public UploadMenuPagingBean saveFileDownload(@RequestBody Map<String, Object> dataUrl,
			Authentication authentication, HttpSession session) {
		List<TblAdminEsbnBeanUpload> adminEsbnBeanUploads = getAllComponentOfXsFile((String) dataUrl.get("dataUri"));
		System.err.println("semua user : " + new Gson().toJson(adminEsbnBeanUploads));
		TokenWrapper tokenWrapper = getMyToken(authentication);
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		HttpRestResponse httpRestResponse = wsBody(wmsParentalWrapper + "/userManagementCtl/add-admin/list",
				adminEsbnBeanUploads, HttpMethod.POST, headerMap);
		UploadMenuPagingBean bean = new UploadMenuPagingBean();
		try {
			ResultUploadDataUserListBean userListBean = mapperJsonToSingleDto(httpRestResponse.getBody(),
					ResultUploadDataUserListBean.class);

			bean.setTotalData(userListBean.getTotalData());
			bean.setTotalAccepted(userListBean.getTotalDataAccept());
			bean.setTotalRejected(userListBean.getTotalDataUnaccept());
			// bean.setUploadContent(userListBean.getContent());
			bean.setUploadBy(authentication.getName());
			session.setAttribute((String) dataUrl.get("name"), userListBean.getContent());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return bean;
	}
}
