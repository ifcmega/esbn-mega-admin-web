package com.bank.mega.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import com.bank.mega.UltimateBase;
import com.bank.mega.bean.admin.TblAdminEsbnBean;
import com.bank.mega.bean.admin.TblAdminEsbnBeanUpload;
import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.bean.principal.BasePrincipalSessions;
import com.bank.mega.security.CustomUserService;
import com.bank.mega.security.HttpRestResponse;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.gson.Gson;

import ma.glasnost.orika.MapperFacade;

@Component
public class BaseService extends UltimateBase {

	@Autowired
	protected SessionRegistry sessionRegistry;

	protected Map<String, String> getMyRequestToken(Authentication authentication) {
		TokenWrapper tokenWrapper = getMyToken(authentication);
		Map<String, String> headerMap = new HashMap<>();
		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());
		return headerMap;
	}

	protected String securityToken(TokenWrapper tokenWrapper) {
		return "Bearer " + tokenWrapper.getAccess_token();
	}

	protected boolean isActiveSession(String userId, Authentication authentication) {
		for (BasePrincipalSessions principal : getAllSessionsGlobal(authentication)) {
			if (principal.getPrincipal().getUserName().equalsIgnoreCase(userId)) {
				return true;
			}
		}
		return false;
	}

	protected List<BasePrincipalSessions> getAllSessionsGlobal(Authentication authentication) {
		List<BasePrincipalSessions> basePrincipalSessions = new ArrayList<>();
		Map<String, String> hash = new HashMap<>();
		hash = getMyRequestToken(authentication);
		HttpRestResponse response = wsBody(wmsParentalWrapper + "/session/get-all-web-session", null, HttpMethod.GET,
				hash);
		List<BasePrincipalSessions> sharedSessions = new ArrayList<>();
		try {
			sharedSessions = mapperJsonToListDto(response.getBody(), BasePrincipalSessions.class);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		basePrincipalSessions.addAll(sharedSessions);
		for (Object principal : sessionRegistry.getAllPrincipals()) {

			List<BasePrincipalSessions> list = new ArrayList<>();
			try {
				System.err.println(new Gson().toJson(sessionRegistry.getAllSessions(principal, true)));
				list = mapperJsonToListDto(new Gson().toJson(sessionRegistry.getAllSessions(principal, true)),
						BasePrincipalSessions.class);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			basePrincipalSessions.add(list.get(0));

		}
		System.err.println("total semua session login : " + basePrincipalSessions.size());
		List<BasePrincipalSessions> internalBsp = new ArrayList<>();
		for (BasePrincipalSessions bps : basePrincipalSessions) {
			BasePrincipalSessions bpsContract = bps;
				try {
					bpsContract.setLastRequestOnFormat(parseStringToOtherStringFormat(bps.getLastRequest(),
							"MMM dd,yyyy HH:mm:ss", "dd/MM/yyyy HH:mm"));
					bpsContract.setFirstLoginInFormat(
							parseStringToOtherStringFormat(bps.getPrincipal().getFirstLogin().replaceAll("\"", ""),
									"MMM dd,yyyy HH:mm:ss", "dd/MM/yyyy HH:mm"));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			
		}
		return basePrincipalSessions;
	}

	protected void checkSessionValidity(Authentication authentication, HttpServletRequest request,
			HttpServletResponse response) {

		System.err.println("masuk ke checkSessionValidity from webservice");
		for (Object principal : sessionRegistry.getAllPrincipals()) {
			if (principal instanceof CustomUserService) {
				CustomUserService service = (CustomUserService) principal;
				System.err.println("cekking : " + new Gson().toJson(service) + "     " + authentication.getName());
				if (service.getUsername().equalsIgnoreCase(authentication.getName())) {
					return;
				}
			}
		}

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		try {
			System.err.println("masuk ke redirect");
			response.sendRedirect("/login");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return;
	}

	protected String checkSessionValidity(String url, Authentication authentication, HttpServletRequest request,
			HttpServletResponse response) {

		System.err.println("masuk ke checkSessionValidity");
		for (Object principal : sessionRegistry.getAllPrincipals()) {
			if (principal instanceof CustomUserService) {
				CustomUserService service = (CustomUserService) principal;
				System.err.println("cekking : " + new Gson().toJson(service) + "     " + authentication.getName());
				if (service.getUsername().equalsIgnoreCase(authentication.getName())) {
					return url;
				}
			}
		}

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login";
	}

	protected void informationHeader(Model model, Authentication authentication) {
		String namaAuthoritie = new Gson().toJson(authentication.getAuthorities()).replace("[", "").replace("]", "");
		Map<String, Object> mapp = mapperJsonToHashMap(new Gson().toJson(authentication.getPrincipal()));
		System.err.println("model mapp : " + new Gson().toJson(authentication));
		model.addAttribute("nama_nasabah", mapp.get("roleDesc"));
		model.addAttribute("userName", mapp.get("userName"));

		String role = (String) mapp.get("roleCode");

		if ((role).equalsIgnoreCase("CS")) {
			getAllMenuAkses(authentication, role, model);
		} else if ((role).equalsIgnoreCase("Admin")) {
			getAllMenuAkses(authentication, role, model);
		} else {
			getAllMenuAkses(authentication, role, model);
		}
		model.addAttribute("kelengkapan_nasabah", mapp.get("roleDesc") + " - " + authentication.getName());
	}

	protected void getAllMenuAkses(Authentication auth, String role, Model model) {
		Map<String, String> headerMap = new HashMap<>();
		List<String> listMenuWs = new ArrayList<>();
		TokenWrapper tokenWrapper = getMyToken(auth);
		headerMap.put("Authorization", tokenWrapper.getToken_type() + " " + tokenWrapper.getAccess_token());

		HttpRestResponse httpRestResponse = wsBody(
				wmsParentalWrapper + "/menuAdminEsbnCtl/check-detail-menu/" + role, null, HttpMethod.GET, headerMap);
		try {
			listMenuWs = mapperJsonToListDto(httpRestResponse.getBody(), String.class);
			for (String data : listMenuWs) {
				model.addAttribute(data, true);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	protected String callOtp(HttpSession session, String menu, String partisi) {

		return (String) session.getAttribute(menu + partisi);
	}

	public List<TblAdminEsbnBeanUpload> getAllComponentOfXsFile(String base64Str) {
		byte[] fileData = DatatypeConverter.parseBase64Binary(base64Str.substring(base64Str.indexOf(",") + 1));
		List<TblAdminEsbnBeanUpload> dataInputBlacklistRequests = null;
		InputStream is = new ByteArrayInputStream(fileData);
		XSSFWorkbook hssfWorkbook;
		try {
			dataInputBlacklistRequests = new ArrayList<>();
			hssfWorkbook = new XSSFWorkbook(is);
			XSSFSheet estabilishedSheet = hssfWorkbook.getSheetAt(0);
			Iterator<Row> rows = estabilishedSheet.iterator();
			List<Row> rowInList = Lists.newArrayList(rows);
			List<Row> rowWithoutHeader = new ArrayList<>();

			int i = 0;
			for (Row row : rowInList) {
				if (i != 0) {
					rowWithoutHeader.add(row);
				}
				i++;
			}

			for (Row row : rowWithoutHeader) {
				TblAdminEsbnBeanUpload adminEsbnBean = new TblAdminEsbnBeanUpload();
				
				if(row.getCell(CellReference.convertColStringToIndex("A"))==null||
						row.getCell(CellReference.convertColStringToIndex("B"))==null||
						row.getCell(CellReference.convertColStringToIndex("C"))==null||
						row.getCell(CellReference.convertColStringToIndex("D"))==null||
						row.getCell(CellReference.convertColStringToIndex("E"))==null||
						row.getCell(CellReference.convertColStringToIndex("F"))==null){
                        continue;					
				}
				
				String userId = row.getCell(CellReference.convertColStringToIndex("A")).getStringCellValue();

				String namaLengkap = row.getCell(CellReference.convertColStringToIndex("B")).getStringCellValue();

				String email = row.getCell(CellReference.convertColStringToIndex("C")).getStringCellValue();

				String isLogin = "No";

				String roles = row.getCell(CellReference.convertColStringToIndex("D")).getStringCellValue();

				String enabled = row.getCell(CellReference.convertColStringToIndex("E")).getStringCellValue();

				String locked = row.getCell(CellReference.convertColStringToIndex("F")).getStringCellValue();

				adminEsbnBean.setUserSid(userId);
				adminEsbnBean.setUserName(namaLengkap);
				adminEsbnBean.setEmail(email);
				System.err.println("is login : " + isLogin + " enabled : " + enabled + " locked : " + locked);
				if (isLogin != null && !Strings.isNullOrEmpty(isLogin) && isLogin.equalsIgnoreCase("Yes")) {
					adminEsbnBean.setIsLogin(true);
				} else if (isLogin != null && !Strings.isNullOrEmpty(isLogin) && isLogin.equalsIgnoreCase("No")) {
					adminEsbnBean.setIsLogin(false);
				}
				adminEsbnBean.setRoles(roles);
				if (enabled != null && !Strings.isNullOrEmpty(enabled) && enabled.equalsIgnoreCase("Yes")) {
					adminEsbnBean.setEnabled(true);
				} else if (enabled != null && !Strings.isNullOrEmpty(enabled) && enabled.equalsIgnoreCase("No")) {
					adminEsbnBean.setEnabled(false);
				}
				if (locked != null && !Strings.isNullOrEmpty(locked) && locked.equalsIgnoreCase("Yes")) {
					adminEsbnBean.setLocked(true);
				} else if (locked != null && !Strings.isNullOrEmpty(locked) && locked.equalsIgnoreCase("No")) {
					adminEsbnBean.setLocked(false);
				}
				dataInputBlacklistRequests.add(adminEsbnBean);
			}
			//
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return dataInputBlacklistRequests;
	}

	private int countFilledRow(Row row) {
		Iterator<Cell> cells = row.cellIterator();
		List<Cell> cellInList = new ArrayList<>();
		cells.forEachRemaining(cellInList::add);
		int countExistColumn = cellInList.size();
		return countExistColumn;
	}

}
