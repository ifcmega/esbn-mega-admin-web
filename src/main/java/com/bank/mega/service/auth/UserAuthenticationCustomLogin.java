package com.bank.mega.service.auth;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.bank.mega.bean.admin.TblAdminEsbnBean;
import com.bank.mega.bean.admin.TblEsbnLoginLogBean;
import com.bank.mega.bean.auth.TokenWrapper;
import com.bank.mega.security.HttpRestResponse;
import com.bank.mega.security.WsResponse;
import com.bank.mega.service.BaseService;
import com.google.gson.Gson;

@Service
public class UserAuthenticationCustomLogin extends BaseService{
	
	public void noteUserLogin(TblAdminEsbnBean adminEsbnBean) {
		TblEsbnLoginLogBean bean = new TblEsbnLoginLogBean();
		bean.setLogUserId(adminEsbnBean.getUserSid());
		bean.setLogUserName(adminEsbnBean.getUserName());
		bean.setLogUserTypeCode(adminEsbnBean.getRoles());
		bean.setLogUserTypeCodeName(adminEsbnBean.getRolesDesc());
		HttpRestResponse response = wsBody(wmsParentalWrapper+"/shared/log/login-user", 
				bean, HttpMethod.POST, null);
		System.err.println("login : " + response.getBody());
	}
	
	public TblAdminEsbnBean findUserBySidAndPassword(String userValidation,String password) {
	    Map<String, String> map = new HashMap<>();
	    map.put("user", userValidation);
	    map.put("password", password);
	    HttpRestResponse response = wsBody(wmsParentalWrapper+"/admin-login-ctl/login-to-admin", 
				map, HttpMethod.POST, null);
		TblAdminEsbnBean tblUserDto = new TblAdminEsbnBean();
		try {
			tblUserDto  = mapperJsonToSingleDto(response.getBody(), TblAdminEsbnBean.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tblUserDto;
	}
}
