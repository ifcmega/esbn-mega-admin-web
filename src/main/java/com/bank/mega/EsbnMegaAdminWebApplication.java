package com.bank.mega;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.bank.mega.bean.admin.TblAdminEsbnBean;
import com.bank.mega.security.CustomUserService;
import com.bank.mega.service.auth.UserAuthenticationCustomLogin;
import com.google.gson.Gson;

@SpringBootApplication
public class EsbnMegaAdminWebApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(EsbnMegaAdminWebApplication.class, args);
	}

	public static boolean isLimitedExceededSession(String username, SessionRegistry sessionRegistry) {

		for (Object principal : sessionRegistry.getAllPrincipals()) {
			System.err.println("ses  " + new Gson().toJson(principal));
			if (principal instanceof CustomUserService) {
				CustomUserService service = (CustomUserService) principal;
				if (service.getUsername().equalsIgnoreCase(username)) {
                    System.err.println("user " + username + " telah memiliki session");
					return false;
				}
			}
		}
		return false;
	}

	@Autowired
	public void authenticationManager(AuthenticationManagerBuilder builder, HttpServletRequest request,
			SessionRegistry sessionRegistry, UserAuthenticationCustomLogin authenticationCustomLogin) throws Exception {
		builder.userDetailsService(new UserDetailsService() {

			@Override
			public UserDetails loadUserByUsername(String userValidation) throws UsernameNotFoundException {
				// check first limited exceeded.
				
			
				if (isLimitedExceededSession(userValidation, sessionRegistry)) {
					return null;
				} else {
					// check second authentication.
					String password = request.getParameter("password");
					TblAdminEsbnBean adminEsbnBean = authenticationCustomLogin.findUserBySidAndPassword(userValidation,
							password);
					if(adminEsbnBean != null) {
						authenticationCustomLogin.noteUserLogin(adminEsbnBean);
					}
					
					return new CustomUserService(adminEsbnBean, password);
				}
			}
		});
	}

}
