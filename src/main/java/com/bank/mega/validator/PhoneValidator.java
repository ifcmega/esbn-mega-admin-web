package com.bank.mega.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class PhoneValidator {
	  public static boolean isValid(String phone) {
		  if(phone==null) {
			  return false;
		  }
		  if(phone.contains(" ")) {
			  return false;
		  }
	        String regex = "^\\+?[0-9. ()-]{10,25}$";
	        Pattern pattern = Pattern.compile(regex);
	        Matcher matcher = pattern.matcher(phone);
		  return matcher.matches();
	   }
	   public static void main(String[] args) {
	      String email = "+6287768248230  ";
	      System.out.println("The E-mail ID is: " + email);
	      System.out.println("Is the above E-mail ID valid? " + isValid(email));
	   }
}
